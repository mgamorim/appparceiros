﻿using EZ.EZControl.ADO.Repositorio;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Servicos.SMS
{
   public class ServicoNotificacao
    {

        private RepositorioNotificacao _repositorio = null;

        public ServicoNotificacao()
        {
            _repositorio = new RepositorioNotificacao(ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString);
        }


        public bool Gravar(Model.SMS.Notificacao dados)
        {
            return _repositorio.GravarNotificacao(dados);

        }

        public List<Model.SMS.Notificacao> Buscar(Model.SMS.Notificacao dados)
        {
            return _repositorio.BuscarNotificacao(dados);

        }

        public bool Atualizar(Model.SMS.Notificacao dados)
        {
            return _repositorio.AtualizaNotificacao(dados);

        }
    }
}
