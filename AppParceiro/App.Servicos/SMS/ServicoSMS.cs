﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Model.SMS;
using App.Servicos.HTTP;
using EZ.EZControl.ADO.Repositorio;
using Newtonsoft.Json;

namespace App.Servicos.SMS
{
    public class ServicoSMS
    {
        private RepositorioSMS _repositorio = null;

        public ServicoSMS()
        {
            _repositorio = new RepositorioSMS(ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString);
        }

        public bool EnviarSMS(Model.SMS.SMS dadosSMS)
        {
            ServicoHTTP _servicoHttp = new ServicoHTTP();
            sendSmsRequest sendSmsRequest = new sendSmsRequest();

            sendSmsRequest.from = dadosSMS.Titulo;
            sendSmsRequest.to = "55" + dadosSMS.Numero;
            sendSmsRequest.schedule = "2014-08-22T14:55:00";
            sendSmsRequest.msg = dadosSMS.Mensagem;
            sendSmsRequest.callbackOption = "NONE";
            sendSmsRequest.id = GravarSMS(dadosSMS).ToString();
            sendSmsRequest.aggregateId = "34500";


            var conteudo = JsonConvert.SerializeObject(
                      new
                      {
                          sendSmsRequest
                      }
                  );


            var result = _servicoHttp.PostHeader("https://api-rest.zenvia.com/services/send-sms", conteudo);

            sendSmsResponse sendSmsResponse = new sendSmsResponse();

            dynamic sendSmsResponse2 = JsonConvert.DeserializeObject(result);


            if (sendSmsResponse2.sendSmsResponse.statusDescription.Value == "Ok")
                return true;
            else
                return false;

            //return true;
        }


        private int GravarSMS(Model.SMS.SMS dados)
        {
           return _repositorio.GravarSMS(dados);

        }





    }
}
