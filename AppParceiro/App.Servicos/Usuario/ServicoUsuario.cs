﻿using App.ADO.Repositorio.Interface;
using App.ADO.Repositorio;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Model.Usuario;
using App.Servicos.HTTP;
using EZ.EZControl.ADO.Repositorio.Interface;
using EZ.EZControl.ADO.Repositorio;

namespace App.Servicos.Usuario
{
     public  class ServicoUsuario
    {
        private IRepositorioUsuario _repositorio = null;
        private IRepositorioDivulgacao _repositorioDivulgacao = null;
        private IRepositorioDashboard _repositorioDashboard = null;
        private IRepositorioConfig _repositorioConfig = null;
        private IRepositorioTreinamento _repositorioTreinamento = null;

        public ServicoUsuario()
        {
            _repositorio  = new RepositorioUsuario(ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString);
            _repositorioDivulgacao = new RepositorioDivulgacao(ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString);
            _repositorioDashboard = new RepositorioDashboard(ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString);
            _repositorioConfig = new RepositorioConfig(ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString);
            _repositorioTreinamento = new RepositorioTreinamento(ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString);
        }


        public string CadastrarUsuario(UsuarioModel model, DadosDivulgacao ModelDivulgue)
        {
            var result = string.Empty;

           var ret = _repositorio.GravarCliente(model, ModelDivulgue);

            if (ret == true)
                result = "Sucesso";

            return result;
        }

        public bool BuscaSlug(string slug)
        {
            var result = new bool();

            result = _repositorio.BuscaSlug(slug);

            return result;
        }

        public UsuarioModel BuscarClientePorEmail(UsuarioModel model)
        {
            var result = new UsuarioModel();

            result = _repositorio.BuscaClientePorEmail(model);

            return result;
        }

        public DadosDivulgacao BuscarDadosDivulgacao(int IdCliente)
        {
            var result = new DadosDivulgacao();

            result = _repositorioDivulgacao.BuscaDivulgacao(IdCliente);

            return result;
        }

        public DashboardModel BuscarDashboard(UsuarioModel usuario)
        {
            var result = new DashboardModel();

            result = _repositorioDashboard.BuscaDashboard(usuario);

            return result;
        }

        public UsuarioModel ValidaUsuarioSenha(UsuarioModel model)
        {
            var result = new UsuarioModel();

            result = _repositorio.ValidaUsuarioSenha(model);

            return result;
        }

        public UsuarioModel RegistraTokenAutenticacao(UsuarioModel model)
        {
            var result = new UsuarioModel();

            result = _repositorio.RegistraTokenAutenticacao(model);

            return result;
        }

        public UsuarioModel AtualizaClienteAtivo(UsuarioModel model)
        {
            var result = new UsuarioModel();

            result = _repositorio.AtualizaClienteAtivo(model);

            return result;
        }

        public UsuarioModel AtualizaDados(UsuarioModel model)
        {
            var result = new UsuarioModel();

            result = _repositorio.AtualizaDados(model);

            return result;
        }

        public UsuarioModel AtualizaSenhaCliente(UsuarioModel model)
        {
            var result = new UsuarioModel();

            result = _repositorio.AtualizaSenhaCliente(model);

            return result;
        }

        public UsuarioModel BuscarClientePorCPF(UsuarioModel model)
        {
            var result = new UsuarioModel();

            result = _repositorio.BuscaClientePorCPF(model);

            return result;
        }

        public UsuarioModel BuscarClientePorTelefone(UsuarioModel model)
        {
            var result = new UsuarioModel();

            result = _repositorio.BuscaClientePorTelefone(model);

            return result;
        }

        public List<ConfigModel> BuscarConfig(ConfigModel model)
        {
            var result = new List<ConfigModel>();

            result = _repositorioConfig.BuscaConfig(model);

            return result;
        }


        public List<TreinamentoModel> BuscarTreinamento(UsuarioModel model)
        {
            var result = new List<TreinamentoModel>();

            result = _repositorioTreinamento.BuscaTreinamento(model);

            return result;
        }



        public bool AtualizaConfig(ConfigRet model)
        {
            var result = new bool();

            result = _repositorioConfig.AtualizaConfig(model);

            return result;
        }

        public string BuscaQrCode(string url)
        {
            var qrcore = string.Empty;

            ServicoHTTP _servicoHttp = new ServicoHTTP();

            qrcore = _servicoHttp.Get(url).Result;

            return qrcore;
        }

    }
}
