﻿using App.Model.Usuario;
using EZ.EZControl.ADO.Repositorio;
using EZ.EZControl.ADO.Repositorio.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Servicos.Usuario
{
    public class ServicoHanking
    {
        private IRepositorioHanking _repositorio = null;



        public ServicoHanking()
        {
            _repositorio = new RepositorioHanking(ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString);
        }



        public List<HankingModel> BuscarHankingVendas()
        {
            return _repositorio.BuscarHankingVendas();

        }

        public List<HankingModel> BuscarHankingVendasIndicados()
        {
            return _repositorio.BuscarHankingVendasIndicados();

        }
        public List<HankingModel> BuscarHankingDestaqueMes()
        {
            return _repositorio.BuscarHankingDestaqueMes();

        }
        


    }
}
