﻿using App.ADO.Repositorio.Interface;
using App.ADO.SqlServer;
using App.Model.Usuario;
using EZ.EZControl.ADO.Repositorio.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio
{
    public class RepositorioDivulgacao : ConexaoSqlServer<DadosDivulgacao>, IRepositorioDivulgacao, ISqlServerInstanciavel<DadosDivulgacao>
    {
        public RepositorioDivulgacao(String connString) : base(connString) { }
        string controle = string.Empty;



        public DadosDivulgacao BuscaDivulgacao(int Id)
        {
            StringBuilder sqlQuery = new StringBuilder();
            DadosDivulgacao model = new DadosDivulgacao();


            sqlQuery.Clear();
            sqlQuery.AppendLine("SELECT  [IdCliente] ");
            sqlQuery.AppendLine(",[LinkVendas] ");
            sqlQuery.AppendLine(",[ImgBanner1] ");
            sqlQuery.AppendLine(",[ImgBanner2] ");
            sqlQuery.AppendLine(",[ImgBanner3] ");
            sqlQuery.AppendLine(",[QrCode] ");
            sqlQuery.AppendLine(",[QrCodeCadastro] ");
            sqlQuery.AppendLine(",[LinkVideo1] ");
            sqlQuery.AppendLine(",[LinkCadastroIndicador] ");
            sqlQuery.AppendLine(",[ImgBannerIndicador] ");
            sqlQuery.AppendFormat(" FROM [Divulgue_tb] with(nolock) WHERE idCliente = {0} ", Id);
            controle = "divulgue";

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                model = Resultado;
            else
                model = null;


            sqlQuery.Clear();
            sqlQuery.AppendFormat(" SELECT TIPO, TEXTO FROM [DivulgueTexto_tb] with(nolock) order by tipo desc");
            controle = "texto";
            SqlCommand sqlCommandTexto = new SqlCommand(sqlQuery.ToString());
            var retornoTexto = ExecuteReader(sqlCommandTexto, this);



            if (retornoTexto)
            {
                model.texto = new List<Texto>();

                foreach (var item in Resultados)
                {
                    model.texto.Add(item.texto[0]);
                }

            }
            else
            {
                model = null;
            }


            return model;
        }

        public DadosDivulgacao Instanciar(SqlDataReader reader)
        {
            var dados = new DadosDivulgacao();
            try
            {
                if (controle == "divulgue")
                {
                    dados.IdCliente = (reader["IdCliente"] != DBNull.Value) ? Convert.ToInt32(reader["IdCliente"]) : 0;
                    dados.LinkVendas = (reader["LinkVendas"] != DBNull.Value) ? reader["LinkVendas"].ToString() : null;
                    dados.ImgBanner1 = (reader["ImgBanner1"] != DBNull.Value) ? reader["ImgBanner1"].ToString() : null;
                    dados.ImgBanner2 = (reader["ImgBanner2"] != DBNull.Value) ? reader["ImgBanner2"].ToString() : null;
                    dados.ImgBanner3 = (reader["ImgBanner3"] != DBNull.Value) ? reader["ImgBanner3"].ToString() : null;
                    dados.QrCode = (reader["QrCode"] != DBNull.Value) ? reader["QrCode"].ToString() : null;
                    dados.QrCodeCadastro = (reader["QrCodeCadastro"] != DBNull.Value) ? reader["QrCodeCadastro"].ToString() : null;
                    dados.LinkVideo1 = (reader["LinkVideo1"] != DBNull.Value) ? reader["LinkVideo1"].ToString() : null;
                    dados.LinkCadastroIndicador = (reader["LinkCadastroIndicador"] != DBNull.Value) ? reader["LinkCadastroIndicador"].ToString() : null;
                    dados.ImgBannerIndicador = (reader["ImgBannerIndicador"] != DBNull.Value) ? reader["ImgBannerIndicador"].ToString() : null;
                }

                if (controle == "texto")
                {
                        dados.texto = new List<Texto>();
                    dados.texto.Add(new Texto { Tipo = reader["TIPO"].ToString().Trim(), texto = reader["TEXTO"].ToString().Trim() });
                }


            }
            catch (Exception exp)
            {

            }
            return dados;
        }
    }
}
