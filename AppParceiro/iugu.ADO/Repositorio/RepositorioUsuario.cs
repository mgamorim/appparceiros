﻿
using App.ADO.Mensageria;
using App.ADO.Repositorio.Interface;
using App.ADO.SqlServer;
using App.Model.Usuario;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.ADO.Repositorio
{
    public class RepositorioUsuario : ConexaoSqlServer<UsuarioModel> , IRepositorioUsuario, ISqlServerInstanciavel<UsuarioModel>
    {
        public RepositorioUsuario(String connString) : base(connString) { }



        public UsuarioModel BuscaClientePorEmail(UsuarioModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("SELECT  usu.id ");
            sqlQuery.AppendLine(",usu.nome ");
            sqlQuery.AppendLine(",caminho  = (select top 1 Caminho from imagem_tb where IdCliente = usu.id order by id desc)  ");
            sqlQuery.AppendLine(",usu.telefone ");
            sqlQuery.AppendLine(",usu.email ");
            sqlQuery.AppendLine(",usu.Cpf ");
            sqlQuery.AppendLine(",usu.datanascimento ");
            sqlQuery.AppendLine(",usu.slug ");
            sqlQuery.AppendLine(",usu.TokenAcesso ");
            sqlQuery.AppendLine(",usu.SenhaAcesso ");
            sqlQuery.AppendLine(",db.nomebanco, db.agencia , db.conta , db.tipoconta  , db.nomefavorecido ");
            sqlQuery.AppendLine(", de.Endereco , de.Cidade, de.Estado, de.Cep ");
            sqlQuery.AppendLine(" FROM [Parceiros].[dbo].[Usuario_tb] usu  LEFT JOIN[Parceiros].[dbo].DadosBancarios_tb db on db.idcliente = usu.id and  db.cpf = usu.cpf  ");
            sqlQuery.AppendLine(" LEFT JOIN[Parceiros].[dbo].DadosEndereco_tb de on de.idcliente = usu.id ");
            sqlQuery.AppendFormat("WHERE usu.email = '{0}' ", model.Email);

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

           var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                model = Resultado;
            else
                model = null;

            return model;
        }

        //public UsuarioModel BuscarImagem(Imagem model)
        //{
        //    StringBuilder sqlQuery = new StringBuilder();

        //    sqlQuery.AppendLine("SELECT  usu.id ");
        //    sqlQuery.AppendLine(",usu.nome ");
        //    sqlQuery.AppendLine(",usu.telefone ");
        //    sqlQuery.AppendLine(",usu.email ");
        //    sqlQuery.AppendLine(",usu.Cpf ");
        //    sqlQuery.AppendLine(",usu.datanascimento ");
        //    sqlQuery.AppendLine(",usu.slug ");
        //    sqlQuery.AppendLine(",usu.TokenAcesso ");
        //    sqlQuery.AppendLine(",usu.SenhaAcesso ");
        //    sqlQuery.AppendLine(",db.nomebanco, db.agencia , db.conta , db.tipoconta  , db.nomefavorecido ");
        //    sqlQuery.AppendLine(" FROM [Parceiros].[dbo].[Usuario_tb] usu  LEFT JOIN[Parceiros].[dbo].DadosBancarios_tb db on db.idcliente = usu.id and  db.cpf = usu.cpf  ");
        //    sqlQuery.AppendFormat("WHERE usu.email = '{0}' ", model.email);

        //    SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

        //    var retorno = ExecuteReader(sqlCommand, this);

        //    if (retorno)
        //        model = Resultado;
        //    else
        //        model = null;

        //    return model;
        //}

        public UsuarioModel ValidaUsuarioSenha(UsuarioModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("SELECT  [id] ");
            sqlQuery.AppendLine(",[nome] ");
            sqlQuery.AppendLine(",[telefone] ");
            sqlQuery.AppendLine(",[email] ");
            sqlQuery.AppendLine(",[Cpf] ");
            sqlQuery.AppendLine(",[datanascimento] ");
            sqlQuery.AppendLine(",[slug] ");
            sqlQuery.AppendLine(",[TokenAcesso] ");
            sqlQuery.AppendFormat(" FROM [Usuario_TB] WHERE email = '{0}' and senhaAcesso = '{1}' and ativo = 1 ", model.Login, model.Senha);

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                model = Resultado;
            else
                model = null;

            return model;
        }


        public UsuarioModel RegistraTokenAutenticacao(UsuarioModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("insert into [HistoricoLogin_tb] (IdCliente, email, DataLogin , TokenAutenticacao) ");
            sqlQuery.AppendLine(" VALUES ");
            sqlQuery.AppendLine("( ");
            sqlQuery.AppendFormat(" {0}, ", model.Id);
            sqlQuery.AppendFormat(" '{0}', ", model.Email);
            sqlQuery.AppendFormat(" '{0}', ", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"));
            sqlQuery.AppendFormat(" '{0}' ", model.TokenAutenticacao);
            sqlQuery.AppendLine(") ");

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                model = Resultado;
            else
                model = null;

            return model;
        }

        public UsuarioModel AtualizaClienteAtivo(UsuarioModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("update [Parceiros].[dbo].[Usuario_tb] ");
            sqlQuery.AppendFormat("set confirmaToken = '{0}' ", model.ConfirmaToken);
            sqlQuery.AppendFormat(",Ativo = {0} ", model.Ativo);
            sqlQuery.AppendFormat(",TokenAcesso = {0} ", model.Token);
            sqlQuery.AppendFormat(" WHERE email = '{0}' ", model.Email);

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                model = Resultado;
            else
                model = null;

            return model;
        }

        public UsuarioModel AtualizaDados(UsuarioModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();


            ///// UPDATE  EM DADOS DO USUARIO

            sqlQuery.Clear();
            sqlQuery.AppendLine("update [Parceiros].[dbo].[Usuario_tb] ");
            sqlQuery.AppendFormat("set nome = '{0}' ", model.Nome);
            sqlQuery.AppendFormat(",Telefone = '{0}' ", model.Telefone.Replace("(", "").Replace(")", "").Replace("-", ""));
            sqlQuery.AppendFormat(",CPF = {0} ", model.CPF == null ? "''" : "'" + model.CPF.Replace(".", "").Replace("-", "") + "'" );

            var montadata = model.DataNascimento == null ? null : model.DataNascimento.Split('/');

            sqlQuery.AppendFormat(",datanascimento = {0} ", model.DataNascimento == null ? "''" : "'" +  montadata[2] + "-" + montadata[1] + "-" + montadata[0] + "'" );
            sqlQuery.AppendFormat(" WHERE email = '{0}' ", model.Email);
            SqlCommand sqlCommandUsuario = new SqlCommand(sqlQuery.ToString());
            var retorno = ExecuteReader(sqlCommandUsuario, this);


            ///// VALIDAÇÃO E INSERT EM DADOS BANCARIOS DO USUARIO

            sqlQuery.Clear();
            sqlQuery.AppendLine("SELECT  usu.id ");
            sqlQuery.AppendLine(",usu.nome ");
            sqlQuery.AppendLine(",caminho  = (select top 1 Caminho from imagem_tb where IdCliente = usu.id order by id desc)  ");
            sqlQuery.AppendLine(",usu.telefone ");
            sqlQuery.AppendLine(",usu.email ");
            sqlQuery.AppendLine(",usu.Cpf ");
            sqlQuery.AppendLine(",usu.datanascimento ");
            sqlQuery.AppendLine(",usu.slug ");
            sqlQuery.AppendLine(",usu.TokenAcesso ");
            sqlQuery.AppendLine(",usu.SenhaAcesso ");
            sqlQuery.AppendLine(",db.nomebanco, db.agencia , db.conta , db.tipoconta  , db.nomefavorecido ");
            sqlQuery.AppendLine(", de.Endereco , de.Cidade, de.Estado, de.Cep ");
            sqlQuery.AppendLine(" FROM [Parceiros].[dbo].[Usuario_tb] usu  LEFT JOIN[Parceiros].[dbo].DadosBancarios_tb db on db.idcliente = usu.id and  db.cpf = usu.cpf  ");
            sqlQuery.AppendLine(" LEFT JOIN[Parceiros].[dbo].DadosEndereco_tb de on de.idcliente = usu.id ");
            sqlQuery.AppendFormat("WHERE usu.email = '{0}' ", model.Email);
            SqlCommand sqlCommandDadosBancarios = new SqlCommand(sqlQuery.ToString());
            var retorno2 = ExecuteReader(sqlCommandDadosBancarios, this);


            if (Resultado.dadosBancarios.Agencia != null)
            {
                sqlQuery.Clear();
                sqlQuery.AppendLine("update DadosBancarios_tb ");
                sqlQuery.AppendFormat("set NomeBanco = '{0}' ", model.dadosBancarios.NomeBanco);
                sqlQuery.AppendFormat(",agencia = '{0}' ", model.dadosBancarios.Agencia);
                sqlQuery.AppendFormat(",conta = '{0}' ", model.dadosBancarios.Conta);
                sqlQuery.AppendFormat(",TipoConta = '{0}' ", model.dadosBancarios.TipoConta);
                sqlQuery.AppendFormat(",NomeFavorecido = '{0}' ", model.dadosBancarios.NomeFavorecido);
                sqlQuery.AppendFormat(" WHERE idcliente = '{0}' ", model.Id);
            }
            else
            {
                sqlQuery.Clear();
                sqlQuery.AppendLine("insert into [DadosBancarios_tb] (IdCliente, NomeBanco, Agencia , Conta, TipoConta, NomeFavorecido, Cpf) ");
                sqlQuery.AppendLine(" VALUES ");
                sqlQuery.AppendLine("( ");
                sqlQuery.AppendFormat(" {0}, ", model.Id);
                sqlQuery.AppendFormat(" '{0}', ", model.dadosBancarios.NomeBanco);
                sqlQuery.AppendFormat(" '{0}', ", model.dadosBancarios.Agencia);
                sqlQuery.AppendFormat(" '{0}', ", model.dadosBancarios.Conta);
                sqlQuery.AppendFormat(" '{0}', ", model.dadosBancarios.TipoConta);
                sqlQuery.AppendFormat(" '{0}', ", model.dadosBancarios.NomeFavorecido);
                sqlQuery.AppendFormat(" '{0}' ", model.CPF);
                sqlQuery.AppendLine(") ");
            }
            SqlCommand sqlCommandDadosUpdateInsert = new SqlCommand(sqlQuery.ToString());
            var retorno3 = ExecuteReader(sqlCommandDadosUpdateInsert, this);




            ///// VALIDAÇÃO E INSERT NA IMAGEM DO USUARIO

            sqlQuery.Clear();
            sqlQuery.AppendLine("SELECT  usu.id ");
            sqlQuery.AppendLine(",usu.nome ");
            sqlQuery.AppendLine(",caminho  = (select top 1 Caminho from imagem_tb where IdCliente = usu.id order by id desc)  ");
            sqlQuery.AppendLine(",usu.telefone ");
            sqlQuery.AppendLine(",usu.email ");
            sqlQuery.AppendLine(",usu.Cpf ");
            sqlQuery.AppendLine(",usu.datanascimento ");
            sqlQuery.AppendLine(",usu.slug ");
            sqlQuery.AppendLine(",usu.TokenAcesso ");
            sqlQuery.AppendLine(",usu.SenhaAcesso ");
            sqlQuery.AppendLine(",db.nomebanco, db.agencia , db.conta , db.tipoconta  , db.nomefavorecido ");
            sqlQuery.AppendLine(", de.Endereco , de.Cidade, de.Estado, de.Cep ");
            sqlQuery.AppendLine(" FROM [Parceiros].[dbo].[Usuario_tb] usu  LEFT JOIN[Parceiros].[dbo].DadosBancarios_tb db on db.idcliente = usu.id and  db.cpf = usu.cpf  ");
            sqlQuery.AppendLine(" LEFT JOIN[Parceiros].[dbo].DadosEndereco_tb de on de.idcliente = usu.id ");
            sqlQuery.AppendFormat("WHERE usu.email = '{0}' ", model.Email);
            SqlCommand sqlCommandImagem = new SqlCommand(sqlQuery.ToString());
            var retorno4 = ExecuteReader(sqlCommandImagem, this);

            if (Resultado.Imagem != null)
            {
                sqlQuery.Clear();
                sqlQuery.AppendLine("update imagem_tb ");
                sqlQuery.AppendFormat("set  ");
                sqlQuery.AppendFormat("Caminho = '{0}' ", model.Imagem);
                sqlQuery.AppendFormat(" WHERE idcliente = {0} ", model.Id);
            }
            else
            {
                sqlQuery.Clear();
                sqlQuery.AppendLine("insert into [imagem_tb] (IdCliente, Caminho) ");
                sqlQuery.AppendLine(" VALUES ");
                sqlQuery.AppendLine("( ");
                sqlQuery.AppendFormat(" {0}, ", model.Id);
                sqlQuery.AppendFormat(" '{0}' ", model.Imagem);
                sqlQuery.AppendLine(") ");
            }
            SqlCommand sqlCommandImagemUpdateInsert = new SqlCommand(sqlQuery.ToString());
            ExecuteNonQuery(ref sqlCommandImagemUpdateInsert);



            ///// VALIDAÇÃO E INSERT NO ENDERECO DO USUARIO

            sqlQuery.Clear();
            sqlQuery.AppendLine("SELECT  usu.id ");
            sqlQuery.AppendLine(",usu.nome ");
            sqlQuery.AppendLine(",caminho  = (select top 1 Caminho from imagem_tb where IdCliente = usu.id order by id desc)  ");
            sqlQuery.AppendLine(",usu.telefone ");
            sqlQuery.AppendLine(",usu.email ");
            sqlQuery.AppendLine(",usu.Cpf ");
            sqlQuery.AppendLine(",usu.datanascimento ");
            sqlQuery.AppendLine(",usu.slug ");
            sqlQuery.AppendLine(",usu.TokenAcesso ");
            sqlQuery.AppendLine(",usu.SenhaAcesso ");
            sqlQuery.AppendLine(",db.nomebanco, db.agencia , db.conta , db.tipoconta  , db.nomefavorecido ");
            sqlQuery.AppendLine(", de.Endereco , de.Cidade, de.Estado, de.Cep ");
            sqlQuery.AppendLine(" FROM [Parceiros].[dbo].[Usuario_tb] usu  LEFT JOIN[Parceiros].[dbo].DadosBancarios_tb db on db.idcliente = usu.id and  db.cpf = usu.cpf  ");
            sqlQuery.AppendLine(" LEFT JOIN[Parceiros].[dbo].DadosEndereco_tb de on de.idcliente = usu.id ");
            sqlQuery.AppendFormat("WHERE usu.email = '{0}' ", model.Email);
            SqlCommand sqlCommandEndereco = new SqlCommand(sqlQuery.ToString());
            var retorno5 = ExecuteReader(sqlCommandEndereco, this);

            if (Resultado.dadosEndereco.Endereco != null)
            {
                sqlQuery.Clear();
                sqlQuery.AppendLine("update DadosEndereco_tb ");
                sqlQuery.AppendFormat("set  ");
                sqlQuery.AppendFormat(" Endereco = '{0}' ", model.dadosEndereco.Endereco);
                sqlQuery.AppendFormat(",Cidade = '{0}' ", model.dadosEndereco.Cidade);
                sqlQuery.AppendFormat(",Estado = '{0}' ", model.dadosEndereco.Estado);
                sqlQuery.AppendFormat(",Cep = '{0}' ", model.dadosEndereco.Cep);
                sqlQuery.AppendFormat(" WHERE idcliente = {0} ", model.Id);
            }
            else
            {
                sqlQuery.Clear();
                sqlQuery.AppendLine("insert into [DadosEndereco_tb] (IdCliente, Endereco, Cidade, Estado, Cep) ");
                sqlQuery.AppendLine(" VALUES ");
                sqlQuery.AppendLine("( ");
                sqlQuery.AppendFormat(" {0}, ", model.Id);
                sqlQuery.AppendFormat(" '{0}', ", model.dadosEndereco.Endereco);
                sqlQuery.AppendFormat(" '{0}', ", model.dadosEndereco.Cidade);
                sqlQuery.AppendFormat(" '{0}', ", model.dadosEndereco.Estado);
                sqlQuery.AppendFormat(" '{0}' ", model.dadosEndereco.Cep);
                sqlQuery.AppendLine(") ");
            }
            SqlCommand sqlCommandEnderecoUpdateInsert = new SqlCommand(sqlQuery.ToString());
            ExecuteNonQuery(ref sqlCommandEnderecoUpdateInsert);



            sqlQuery.Clear();
            sqlQuery.AppendLine("SELECT  usu.id ");
            sqlQuery.AppendLine(",usu.nome ");
            sqlQuery.AppendLine(",caminho  = (select top 1 Caminho from imagem_tb where IdCliente = usu.id order by id desc)  ");
            sqlQuery.AppendLine(",usu.telefone ");
            sqlQuery.AppendLine(",usu.email ");
            sqlQuery.AppendLine(",usu.Cpf ");
            sqlQuery.AppendLine(",usu.datanascimento ");
            sqlQuery.AppendLine(",usu.slug ");
            sqlQuery.AppendLine(",usu.TokenAcesso ");
            sqlQuery.AppendLine(",usu.SenhaAcesso ");
            sqlQuery.AppendLine(",db.nomebanco, db.agencia , db.conta , db.tipoconta  , db.nomefavorecido ");
            sqlQuery.AppendLine(", de.Endereco , de.Cidade, de.Estado, de.Cep ");
            sqlQuery.AppendLine(" FROM [Parceiros].[dbo].[Usuario_tb] usu  LEFT JOIN[Parceiros].[dbo].DadosBancarios_tb db on db.idcliente = usu.id and  db.cpf = usu.cpf  ");
            sqlQuery.AppendLine(" LEFT JOIN[Parceiros].[dbo].DadosEndereco_tb de on de.idcliente = usu.id ");
            sqlQuery.AppendFormat("WHERE usu.email = '{0}' ", model.Email);
            SqlCommand sqlCommandFinal = new SqlCommand(sqlQuery.ToString());
            var retorno6 = ExecuteReader(sqlCommandFinal, this);



            if (retorno6)
                model = Resultado;
            else
                model = null;


            return model;
        }


        public UsuarioModel AtualizaSenhaCliente(UsuarioModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("update [Parceiros].[dbo].[Usuario_tb] ");
            sqlQuery.AppendFormat("set SenhaAcesso = '{0}' ", model.Senha);
            sqlQuery.AppendFormat(" WHERE email = '{0}' ", model.Email);

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                model = Resultado;
            else
                model = null;

            return model;
        }



        public UsuarioModel BuscaClientePorCPF(UsuarioModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("SELECT  [id] ");
            sqlQuery.AppendLine(",[nome] ");
            sqlQuery.AppendLine(",[telefone] ");
            sqlQuery.AppendLine(",[email] ");
            sqlQuery.AppendLine(",[Cpf] ");
            sqlQuery.AppendLine(",[datanascimento] ");
            sqlQuery.AppendLine(",[slug] ");
            sqlQuery.AppendFormat(" FROM [Usuario_TB] WHERE Cpf = '{0}' ", model.CPF.Replace(".","").Replace("-",""));

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                model = Resultado;
            else
                model = null;

            return model;
        }

        public UsuarioModel BuscaClientePorTelefone(UsuarioModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("SELECT  [id] ");
            sqlQuery.AppendLine(",[nome] ");
            sqlQuery.AppendLine(",[telefone] ");
            sqlQuery.AppendLine(",[email] ");
            sqlQuery.AppendLine(",[Cpf] ");
            sqlQuery.AppendLine(",[datanascimento] ");
            sqlQuery.AppendLine(",[slug] ");
            sqlQuery.AppendFormat(" FROM [Usuario_TB] WHERE telefone = '{0}' ", model.Telefone.Replace("(","").Replace(")","").Replace("-",""));

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                model = Resultado;
            else
                model = null;

            return model;
        }

        public bool BuscaSlug(string slug)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("SELECT  [id] ");
            sqlQuery.AppendLine(",[nome] ");
            sqlQuery.AppendLine(",[telefone] ");
            sqlQuery.AppendLine(",[email] ");
            sqlQuery.AppendLine(",[Cpf] ");
            sqlQuery.AppendLine(",[datanascimento] ");
            sqlQuery.AppendLine(",[slug] ");
            sqlQuery.AppendFormat(" FROM [Usuario_TB] WHERE slug = '{0}' ", slug);

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            var retorno = ExecuteReader(sqlCommand, this);

            return retorno;
        }

        public bool GravarCliente(UsuarioModel model, DadosDivulgacao divulgue)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Clear();

            try
            {
                sqlQuery.Clear();
                sqlQuery.AppendLine("INSERT INTO Usuario_TB ");
                sqlQuery.AppendLine(" ( ");
                sqlQuery.AppendLine("Nome, ");
                sqlQuery.AppendLine("Telefone, ");
                sqlQuery.AppendLine("email, ");
                sqlQuery.AppendLine("CPF, ");
                sqlQuery.AppendLine("DataNascimento, ");
                sqlQuery.AppendLine("DataCadastro, ");
                sqlQuery.AppendLine("Ativo, ");
                sqlQuery.AppendLine("Indicador, ");
                sqlQuery.AppendLine("SenhaAcesso, ");
                sqlQuery.AppendLine("TokenAcesso, ");
                sqlQuery.AppendLine("ConfirmaToken, ");
                sqlQuery.AppendLine("Slug ");

                sqlQuery.AppendLine(") ");
                sqlQuery.AppendLine("VALUES ");
                sqlQuery.AppendLine("( ");


                //var montadata = model.DataNascimento.Split('/');

                sqlQuery.AppendFormat(" '{0}' , ", model.Nome);
                sqlQuery.AppendFormat(" '{0}' , ", model.Telefone.Replace("(", "").Replace(")", "").Replace("-", "").Trim());
                sqlQuery.AppendFormat(" '{0}' , ", model.Email);
                //sqlQuery.AppendFormat(" '{0}' , ", model.CPF.Replace(".", "").Replace("-", "").Trim());
                sqlQuery.AppendFormat(" NULL , ");
                //sqlQuery.AppendFormat(" NULL , ", montadata[2] + "-" + montadata[1] + "-" + montadata[0]);
                sqlQuery.AppendFormat(" NULL , ");
                sqlQuery.AppendFormat(" '{0}' , ", DateTime.Now);
                sqlQuery.AppendFormat("  {0} , ", 0);
                sqlQuery.AppendFormat(" '{0}' , ", model.Indicador == null ? null : model.Indicador);
                sqlQuery.AppendFormat(" '{0}' , ", model.Senha);
                sqlQuery.AppendFormat("  {0},  ", model.Token);
                sqlQuery.AppendFormat("  'N',  ");
                sqlQuery.AppendFormat(" '{0}'  ", model.Slug);
                sqlQuery.AppendLine(")");
                sqlQuery.AppendFormat("SELECT convert(varchar(10),id) as 'ID' from Usuario_TB WHERE EMAIL = '{0}'", model.Email);

                sqlQuery.AppendFormat(" insert into [Configuracao_tb] (email, tipo) values ('{0}' , 'sms' ) ", model.Email);
                sqlQuery.AppendFormat(" insert into [Configuracao_tb] (email, tipo) values ('{0}' , 'vibracao' ) ", model.Email);
                sqlQuery.AppendFormat(" insert into [Configuracao_tb] (email, tipo) values ('{0}' , 'sons' ) ", model.Email);

               



                SqlCommand sqlCommand2 = new SqlCommand(sqlQuery.ToString());

                var resultadoId = "";
                ExecuteQueryText(sqlCommand2, out resultadoId);


                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO Divulgue_tb ");
                sqlQuery.AppendLine("( ");
                sqlQuery.AppendLine("IdCliente, ");
                sqlQuery.AppendLine("LinkVendas, ");
                sqlQuery.AppendLine("ImgBanner1, ");
                sqlQuery.AppendLine("ImgBanner2, ");
                sqlQuery.AppendLine("ImgBanner3, ");
                sqlQuery.AppendLine("QrCode, ");
                sqlQuery.AppendLine("QrCodeCadastro, ");
                sqlQuery.AppendLine("LinkVideo1, ");
                sqlQuery.AppendLine("LinkCadastroIndicador, ");
                sqlQuery.AppendLine("ImgBannerIndicador "); 
                sqlQuery.AppendLine(") ");
                sqlQuery.AppendLine("VALUES ");
                sqlQuery.AppendLine("( ");
                sqlQuery.AppendFormat(" {0} , ", resultadoId);
                sqlQuery.AppendFormat(" '{0}' , ", divulgue.LinkVendas);
                sqlQuery.AppendFormat(" '{0}' , ", divulgue.ImgBanner1);
                sqlQuery.AppendFormat(" '{0}' , ", divulgue.ImgBanner2);
                sqlQuery.AppendFormat(" '{0}' , ", divulgue.ImgBanner3);
                sqlQuery.AppendFormat(" '{0}' , ", divulgue.QrCode);
                sqlQuery.AppendFormat(" '{0}' , ", divulgue.QrCodeCadastro);
                sqlQuery.AppendFormat(" '{0}' , ", divulgue.LinkVideo1);
                sqlQuery.AppendFormat(" '{0}' , ", divulgue.LinkCadastroIndicador);
                sqlQuery.AppendFormat(" '{0}' ", divulgue.ImgBannerIndicador);
                sqlQuery.AppendLine(")");

                sqlQuery.AppendFormat(" insert into Notificacao_tb (email, descricao, links, data ,  tipo, lida) values ('{0}', 'Olá, Seja bem vindo ! Assista nosso vídeo explicativo do App em Treinamentos clique no link abaixo ou navegue até Treinamentos !', 'https://app.evida.online/#/help', GETDATE() , '1' , 'false' ) ",model.Email);


                SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

                return ExecuteNonQuery(ref sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UsuarioModel Instanciar(SqlDataReader reader)
        {
            var dados = new UsuarioModel();
            try
            {
                dados.Email = (reader["email"] != DBNull.Value) ? reader["email"].ToString() : null;
                dados.Nome = (reader["nome"] != DBNull.Value) ? reader["nome"].ToString() : null;
                dados.Imagem = (reader["caminho"] != DBNull.Value) ? reader["caminho"].ToString() : null;
                dados.CPF = (reader["Cpf"] != DBNull.Value) ? reader["Cpf"].ToString() : null;
                dados.DataNascimento = (reader["datanascimento"] != DBNull.Value) ? Convert.ToDateTime(reader["datanascimento"]).ToString("dd/MM/yyyy") : null;
                dados.Telefone = (reader["Telefone"] != DBNull.Value) ? reader["Telefone"].ToString() : null;
                dados.Slug = (reader["Slug"] != DBNull.Value) ? reader["Slug"].ToString() : null;
                dados.Token = (reader["TokenAcesso"] != DBNull.Value) ? reader["TokenAcesso"].ToString() : null;
                dados.Senha = (reader["SenhaAcesso"] != DBNull.Value) ? reader["SenhaAcesso"].ToString() : null;
                dados.Id = (reader["Id"] != DBNull.Value) ? Convert.ToInt32(reader["Id"]) : 0;
                
                dados.dadosBancarios = new DadosBancarios();
                dados.dadosBancarios.Agencia = (reader["agencia"] != DBNull.Value) ? reader["agencia"].ToString() : null;
                dados.dadosBancarios.NomeBanco = (reader["nomebanco"] != DBNull.Value) ? reader["nomebanco"].ToString() : null;
                dados.dadosBancarios.Conta = (reader["conta"] != DBNull.Value) ? reader["conta"].ToString() : null;
                dados.dadosBancarios.TipoConta = (reader["tipoconta"] != DBNull.Value) ? reader["tipoconta"].ToString() : null;
                dados.dadosBancarios.NomeFavorecido = (reader["nomefavorecido"] != DBNull.Value) ? reader["nomefavorecido"].ToString() : null;

                dados.dadosEndereco = new DadosEndereco();
                dados.dadosEndereco.Endereco = (reader["Endereco"] != DBNull.Value) ? reader["Endereco"].ToString() : null;
                dados.dadosEndereco.Cidade = (reader["Cidade"] != DBNull.Value) ? reader["Cidade"].ToString() : null;
                dados.dadosEndereco.Estado = (reader["Estado"] != DBNull.Value) ? reader["Estado"].ToString() : null;
                dados.dadosEndereco.Cep = (reader["Cep"] != DBNull.Value) ? reader["Cep"].ToString() : null;

            }
            catch (Exception exp)
            {
               
            }
            return dados;
        }

       
    }
}
