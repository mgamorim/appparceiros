﻿using App.ADO.SqlServer;
using App.Model.Usuario;
using EZ.EZControl.ADO.Repositorio.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio
{
    public class RepositorioHanking : ConexaoSqlServer<HankingModel>, IRepositorioHanking, ISqlServerInstanciavel<HankingModel>
    {
        public RepositorioHanking(String connString) : base(connString) { }



        

        public List<HankingModel> BuscarHankingVendas()
        {
            List<HankingModel> lista = new List<HankingModel>();
            StringBuilder sqlQuery = new StringBuilder();

            try
            {





                sqlQuery.Clear();
                sqlQuery.AppendFormat("select slug  from [dbo].[Usuario_tb] with(nolock) where ativo = 1");
                SqlCommand sqlCommandPUsuario = new SqlCommand(sqlQuery.ToString());
                ConnString = ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString;

                var retornoSlugs = ExecuteReaderList(sqlCommandPUsuario, this, "slug");


                foreach (var item in retornoSlugs)
                {


                    sqlQuery.Clear();
                    sqlQuery.AppendFormat("select idproposta from Simulacao.dbo.Simulacao_tb with(nolock) where 1=1 and  parceiro = '{0}' and idproposta is not null  and idproposta <> '' ", item);
                    SqlCommand sqlCommandSimulacao = new SqlCommand(sqlQuery.ToString());
                    ConnString = ConfigurationManager.ConnectionStrings["SimuladorDB"].ConnectionString;

                    var retornoQtdPropostasParceiro = ExecuteReaderList(sqlCommandSimulacao, this, "idproposta");

                    int virgula = retornoQtdPropostasParceiro.Count > 1 ? retornoQtdPropostasParceiro.Count - 1 : retornoQtdPropostasParceiro.Count;

                    string Listpropostas = string.Empty;
                    for (int i = 0; i < retornoQtdPropostasParceiro.Count; i++)
                    {
                        Listpropostas += retornoQtdPropostasParceiro[i];
                        Listpropostas += virgula != retornoQtdPropostasParceiro.Count ? "," : "";
                        virgula++;
                    }



                    sqlQuery.Clear();
                    sqlQuery.AppendFormat("select slug from [dbo].[Usuario_tb] where indicador = '{0}' ", item);
                    SqlCommand sqlCommandPUsuarioiNDICADO = new SqlCommand(sqlQuery.ToString());
                    ConnString = ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString;

                    var retornoSlugsIndicado = ExecuteReaderList(sqlCommandPUsuarioiNDICADO, this, "slug");

                    int virgula1 = retornoSlugsIndicado.Count > 1 ? retornoSlugsIndicado.Count - 1 : retornoSlugsIndicado.Count;

                    string ListSlugIndicado = string.Empty;
                    for (int i = 0; i < retornoSlugsIndicado.Count; i++)
                    {
                        ListSlugIndicado += retornoSlugsIndicado[i];
                        ListSlugIndicado += virgula1 != retornoSlugsIndicado.Count ? "," : "";
                        virgula1++;
                    }



                    sqlQuery.Clear();
                    sqlQuery.AppendFormat("select idproposta from Simulacao.dbo.Simulacao_tb with(nolock)   where 1=1 and  parceiro in ('{0}') and idproposta is not null  and idproposta <> '' ", ListSlugIndicado);
                    SqlCommand sqlCommandListPropostasParceiro = new SqlCommand(sqlQuery.ToString());
                    ConnString = ConfigurationManager.ConnectionStrings["SimuladorDB"].ConnectionString;

                    var retornoPropostasIndicados = ExecuteReaderList(sqlCommandListPropostasParceiro, this, "idproposta");

                    int virgula02 = retornoPropostasIndicados.Count > 1 ? retornoPropostasIndicados.Count - 1 : retornoPropostasIndicados.Count;

                    string ListpropostasIndicados = string.Empty;
                    for (int i = 0; i < retornoPropostasIndicados.Count; i++)
                    {
                        ListpropostasIndicados += retornoPropostasIndicados[i];
                        ListpropostasIndicados += virgula02 != retornoPropostasIndicados.Count ? "," : "";
                        virgula02++;
                    }


                    sqlQuery.Clear();
                    sqlQuery.AppendFormat("SELECT 'TotalComissao' = (select convert(Numeric(10,2),ISNULL(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*40/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and status = 'Pago') +  ", Listpropostas == "" ? "0" : Listpropostas);
                    sqlQuery.AppendFormat(" (select convert(Numeric(10,2),ISNULL(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*10/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and statusParceiro = 'Pago'), ", ListpropostasIndicados == "" ? "0" : ListpropostasIndicados);
                    sqlQuery.AppendFormat(" '{0}' as 'TotalIndicados',", retornoSlugsIndicado.Count);
                    sqlQuery.AppendFormat(" '{0}' as 'Nome' ", item);

                    SqlCommand sqlCommandPagamento = new SqlCommand(sqlQuery.ToString());
                    ConnString = ConfigurationManager.ConnectionStrings["PagamentoDB"].ConnectionString;
                    var retornoPagamento = ExecuteReader(sqlCommandPagamento, this);

                    // var teste = (decimal)("296.00");

                    lista.Add(Resultado);
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }



            return lista;
        }





        public List<HankingModel> BuscarHankingDestaqueMes()
        {
            List<HankingModel> lista = new List<HankingModel>();
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.Clear();
            sqlQuery.AppendFormat("select slug  from [dbo].[Usuario_tb] with(nolock) where ativo = 1");
            SqlCommand sqlCommandPUsuario = new SqlCommand(sqlQuery.ToString());
            ConnString = ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString;

            var retornoSlugs = ExecuteReaderList(sqlCommandPUsuario, this, "slug");


            foreach (var item in retornoSlugs)
            {


                sqlQuery.Clear();
                sqlQuery.AppendFormat("select idproposta from Simulacao.dbo.Simulacao_tb with(nolock) where 1=1 and  parceiro = '{0}' and idproposta is not null and MONTH(datasimulacao) = MONTH(GETDATE())  and idproposta <> '' ", item);
                SqlCommand sqlCommandSimulacao = new SqlCommand(sqlQuery.ToString());
                ConnString = ConfigurationManager.ConnectionStrings["SimuladorDB"].ConnectionString;

                var retornoQtdPropostasParceiro = ExecuteReaderList(sqlCommandSimulacao, this, "idproposta");

                int virgula = retornoQtdPropostasParceiro.Count > 1 ? retornoQtdPropostasParceiro.Count - 1 : retornoQtdPropostasParceiro.Count;

                string Listpropostas = string.Empty;
                for (int i = 0; i < retornoQtdPropostasParceiro.Count; i++)
                {
                    Listpropostas += retornoQtdPropostasParceiro[i];
                    Listpropostas += virgula != retornoQtdPropostasParceiro.Count ? "," : "";
                    virgula++;
                }



                sqlQuery.Clear();
                sqlQuery.AppendFormat("select slug from [dbo].[Usuario_tb] where indicador = '{0}' ", item);
                SqlCommand sqlCommandPUsuarioiNDICADO = new SqlCommand(sqlQuery.ToString());
                ConnString = ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString;

                var retornoSlugsIndicado = ExecuteReaderList(sqlCommandPUsuarioiNDICADO, this, "slug");

                int virgula1 = retornoSlugsIndicado.Count > 1 ? retornoSlugsIndicado.Count - 1 : retornoSlugsIndicado.Count;

                string ListSlugIndicado = string.Empty;
                for (int i = 0; i < retornoSlugsIndicado.Count; i++)
                {
                    ListSlugIndicado += retornoSlugsIndicado[i];
                    ListSlugIndicado += virgula1 != retornoSlugsIndicado.Count ? "," : "";
                    virgula1++;
                }



                sqlQuery.Clear();
                sqlQuery.AppendFormat("select idproposta from Simulacao.dbo.Simulacao_tb with(nolock)  where 1=1 and  parceiro in ('{0}') and idproposta is not null and MONTH(datasimulacao) = MONTH(GETDATE()) and idproposta <> '' ", ListSlugIndicado);
                SqlCommand sqlCommandListPropostasParceiro = new SqlCommand(sqlQuery.ToString());
                ConnString = ConfigurationManager.ConnectionStrings["SimuladorDB"].ConnectionString;

                var retornoPropostasIndicados = ExecuteReaderList(sqlCommandListPropostasParceiro, this, "idproposta");

                int virgula02 = retornoPropostasIndicados.Count > 1 ? retornoPropostasIndicados.Count - 1 : retornoPropostasIndicados.Count;

                string ListpropostasIndicados = string.Empty;
                for (int i = 0; i < retornoPropostasIndicados.Count; i++)
                {
                    ListpropostasIndicados += retornoPropostasIndicados[i];
                    ListpropostasIndicados += virgula02 != retornoPropostasIndicados.Count ? "," : "";
                    virgula02++;
                }


                sqlQuery.Clear();
                sqlQuery.AppendFormat("SELECT 'TotalComissao' = (select convert(Numeric(10,2),ISNULL(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*40/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and status = 'Pago') +  ", Listpropostas == "" ? "0" : Listpropostas);
                sqlQuery.AppendFormat(" (select convert(Numeric(10,2),ISNULL(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*10/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and statusParceiro = 'Pago'), ", ListpropostasIndicados == "" ? "0" : ListpropostasIndicados);
                sqlQuery.AppendFormat(" '{0}' as 'TotalIndicados',", retornoSlugsIndicado.Count);
                sqlQuery.AppendFormat(" '{0}' as 'Nome' ", item);

                SqlCommand sqlCommandPagamento = new SqlCommand(sqlQuery.ToString());
                ConnString = ConfigurationManager.ConnectionStrings["PagamentoDB"].ConnectionString;
                var retornoPagamento = ExecuteReader(sqlCommandPagamento, this);


                lista.Add(Resultado);
            }



            return lista;
        }


        public List<HankingModel> BuscarHankingVendasIndicados()
        {
            List<HankingModel> lista = new List<HankingModel>();
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.Clear();
            sqlQuery.AppendFormat("select slug  from [dbo].[Usuario_tb] with(nolock) where ativo = 1");
            SqlCommand sqlCommandPUsuario = new SqlCommand(sqlQuery.ToString());
            ConnString = ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString;

            var retornoSlugs = ExecuteReaderList(sqlCommandPUsuario, this, "slug");


            foreach (var item in retornoSlugs)
            {


                //sqlQuery.Clear();
                //sqlQuery.AppendFormat("select idproposta from Simulacao.dbo.Simulacao_tb with(nolock) where 1=1 and  parceiro = '{0}' and idproposta is not null  and idproposta <> '' ", item);
                //SqlCommand sqlCommandSimulacao = new SqlCommand(sqlQuery.ToString());
                //ConnString = ConfigurationManager.ConnectionStrings["SimuladorDB"].ConnectionString;

                //var retornoQtdPropostasParceiro = ExecuteReaderList(sqlCommandSimulacao, this, "idproposta");

                //int virgula = retornoQtdPropostasParceiro.Count > 1 ? retornoQtdPropostasParceiro.Count - 1 : retornoQtdPropostasParceiro.Count;

                //string Listpropostas = string.Empty;
                //for (int i = 0; i < retornoQtdPropostasParceiro.Count; i++)
                //{
                //    Listpropostas += retornoQtdPropostasParceiro[i];
                //    Listpropostas += virgula != retornoQtdPropostasParceiro.Count ? "," : "";
                //    virgula++;
                //}



                sqlQuery.Clear();
                sqlQuery.AppendFormat("select slug from [dbo].[Usuario_tb] where indicador = '{0}' ", item);
                SqlCommand sqlCommandPUsuarioiNDICADO = new SqlCommand(sqlQuery.ToString());
                ConnString = ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString;

                var retornoSlugsIndicado = ExecuteReaderList(sqlCommandPUsuarioiNDICADO, this, "slug");

                int virgula1 = retornoSlugsIndicado.Count > 1 ? retornoSlugsIndicado.Count - 1 : retornoSlugsIndicado.Count;

                string ListSlugIndicado = string.Empty;
                for (int i = 0; i < retornoSlugsIndicado.Count; i++)
                {
                    ListSlugIndicado += retornoSlugsIndicado[i];
                    ListSlugIndicado += virgula1 != retornoSlugsIndicado.Count ? "," : "";
                    virgula1++;
                }



                sqlQuery.Clear();
                sqlQuery.AppendFormat("select idproposta from Simulacao.dbo.Simulacao_tb with(nolock)   where 1=1 and  parceiro in ('{0}') and idproposta is not null  and idproposta <> '' ", ListSlugIndicado);
                SqlCommand sqlCommandListPropostasParceiro = new SqlCommand(sqlQuery.ToString());
                ConnString = ConfigurationManager.ConnectionStrings["SimuladorDB"].ConnectionString;

                var retornoPropostasIndicados = ExecuteReaderList(sqlCommandListPropostasParceiro, this, "idproposta");

                int virgula02 = retornoPropostasIndicados.Count > 1 ? retornoPropostasIndicados.Count - 1 : retornoPropostasIndicados.Count;

                string ListpropostasIndicados = string.Empty;
                for (int i = 0; i < retornoPropostasIndicados.Count; i++)
                {
                    ListpropostasIndicados += retornoPropostasIndicados[i];
                    ListpropostasIndicados += virgula02 != retornoPropostasIndicados.Count ? "," : "";
                    virgula02++;
                }


                sqlQuery.Clear();
               // sqlQuery.AppendFormat("SELECT 'TotalComissao' = (select convert(Numeric(10,2),ISNULL(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*40/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and status = 'Pago') +  ", Listpropostas == "" ? "0" : Listpropostas);
                sqlQuery.AppendFormat(" SELECT 'TotalComissao' = (select convert(Numeric(10,2),ISNULL(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*10/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and statusParceiro = 'Pago'), ", ListpropostasIndicados == "" ? "0" : ListpropostasIndicados);
                sqlQuery.AppendFormat(" '{0}' as 'TotalIndicados',", retornoSlugsIndicado.Count);
                sqlQuery.AppendFormat(" '{0}' as 'Nome' ", item);

                SqlCommand sqlCommandPagamento = new SqlCommand(sqlQuery.ToString());
                ConnString = ConfigurationManager.ConnectionStrings["PagamentoDB"].ConnectionString;
                var retornoPagamento = ExecuteReader(sqlCommandPagamento, this);


                lista.Add(Resultado);
            }



            return lista;
        }







        public HankingModel Instanciar(SqlDataReader reader)
        {
            var dados = new HankingModel();


            dados.Nick = (reader["Nome"] != DBNull.Value) ? reader["Nome"].ToString() : dados.Nick;
            dados.TotalComissao = (reader["TotalComissao"] != DBNull.Value) ? (decimal)(reader["TotalComissao"]) : dados.TotalComissao;
            dados.TotalIndicados = (reader["TotalIndicados"] != DBNull.Value) ? Convert.ToInt32(reader["TotalIndicados"]) : dados.TotalIndicados;



            return dados;
        }








    }
}
