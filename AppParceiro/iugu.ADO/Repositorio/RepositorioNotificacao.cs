﻿using App.ADO.SqlServer;
using App.Model.SMS;
using EZ.EZControl.ADO.Repositorio.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio
{
  public  class RepositorioNotificacao : ConexaoSqlServer<Notificacao>, IRepositorioNotificacao, ISqlServerInstanciavel<Notificacao>
    {
        public RepositorioNotificacao(String connString) : base(connString) { }


        public bool GravarNotificacao(Notificacao model)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Clear();

            try
            {
                sqlQuery.AppendLine("INSERT INTO [Notificacao_tb] ");
                sqlQuery.AppendLine(" ( ");
                sqlQuery.AppendLine("email, ");
                sqlQuery.AppendLine("descricao, ");
                sqlQuery.AppendLine("links, ");
                sqlQuery.AppendLine("tipo, ");
                sqlQuery.AppendLine("Data ");

                sqlQuery.AppendLine(") ");
                sqlQuery.AppendLine("VALUES ");
                sqlQuery.AppendLine("( ");
                sqlQuery.AppendFormat("  '{0}',  ", model.Email);
                sqlQuery.AppendFormat("  '{0}',  ", model.Descricao);
                sqlQuery.AppendFormat("  '{0}',  ", model.Links);
                sqlQuery.AppendFormat("  '{0}',  ", model.Tipo);
                sqlQuery.AppendFormat("  '{0}'  ", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"));
                sqlQuery.AppendLine(") ");

                SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

                return ExecuteNonQuery(ref sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool AtualizaNotificacao(Notificacao model)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Clear();

            try
            {
                sqlQuery.AppendFormat("update [dbo].[Notificacao_tb] set lida = 'true' where 1=1 and email = '{0}' ", model.Email);

                SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

                return ExecuteNonQuery(ref sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<Notificacao> BuscarNotificacao(Notificacao model)
        {
            StringBuilder sqlQuery = new StringBuilder();
            List<Notificacao> lista = new List<Notificacao>();
            sqlQuery.Clear();

            try
            {
                sqlQuery.AppendFormat("SELECT top 10 id, EMAIL, DESCRICAO, LINKS, TIPO, DATA, LIDA FROM [Notificacao_tb] WHERE EMAIL in ('{0}','GERAL')  order by id desc", model.Email);
                SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

                var retorno = ExecuteReader(sqlCommand, this);

                if (retorno)
                {
                    lista = Resultados;
                }
                else
                {
                    lista = null;
                }

                return lista;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Notificacao Instanciar(SqlDataReader reader)
        {
            var dados = new Notificacao();
            try
            {
                dados.Id = (reader["id"] != DBNull.Value) ? Convert.ToInt32(reader["id"]) : 0;
                dados.Email = (reader["email"] != DBNull.Value) ? reader["email"].ToString() : null;
                dados.Descricao = (reader["Descricao"] != DBNull.Value) ? reader["Descricao"].ToString() : null;
                dados.Links = (reader["links"] != DBNull.Value) ? reader["links"].ToString() : null;
                dados.Tipo = (reader["Tipo"] != DBNull.Value) ? reader["Tipo"].ToString() : null;
                dados.Data = (reader["Data"] != DBNull.Value) ? Convert.ToDateTime(reader["Data"].ToString()).ToString("dd/MM/yyyy HH:mm:ss") : null;
                dados.Lida = (reader["Lida"] != DBNull.Value) ? reader["Lida"].ToString().Trim() : null;

            }
            catch (Exception exp)
            {

            }
            return dados;
        }
    }
}
