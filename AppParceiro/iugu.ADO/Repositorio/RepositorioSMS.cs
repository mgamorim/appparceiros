﻿using App.ADO.SqlServer;
using App.Model.SMS;
using EZ.EZControl.ADO.Repositorio.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio
{
   public class RepositorioSMS : ConexaoSqlServer<SMS>, IRepositorioSMS, ISqlServerInstanciavel<SMS>
    {

        public RepositorioSMS(String connString) : base(connString) { }


        public int GravarSMS(SMS model)
        {
            int retorno = 0;
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Clear();

            try
            {
                sqlQuery.AppendLine("INSERT INTO SMS_tb ");
                sqlQuery.AppendLine(" ( ");
                sqlQuery.AppendLine("Numero, ");
                sqlQuery.AppendLine("texto, ");
                sqlQuery.AppendLine("Status, ");
                sqlQuery.AppendLine("Data ");

                sqlQuery.AppendLine(") ");
                sqlQuery.AppendLine("VALUES ");
                sqlQuery.AppendLine("( ");
                sqlQuery.AppendFormat("  '{0}',  ",model.Numero);
                sqlQuery.AppendFormat("  '{0}',  ", model.Mensagem);
                sqlQuery.AppendFormat("  '{0}',  ","Enviada");
                sqlQuery.AppendFormat("  '{0}'  ", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"));
                sqlQuery.AppendLine(") select top 1 CONVERT(VARCHAR(20),ID) AS 'teste' from SMS_tb order by id desc");

                SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

                var ret = ExecuteQueryText(sqlCommand , out string ee);

                return Convert.ToInt32(ee);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


       


        public SMS Instanciar(SqlDataReader reader)
        {
            throw new NotImplementedException();
        }
    }
}
