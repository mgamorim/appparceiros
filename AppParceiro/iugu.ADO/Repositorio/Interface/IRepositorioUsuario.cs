﻿

using App.Model.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.ADO.Repositorio.Interface
{
    public interface IRepositorioUsuario
    {
       bool GravarCliente(UsuarioModel model, DadosDivulgacao divulgue);
        UsuarioModel BuscaClientePorEmail(UsuarioModel model);
        //UsuarioModel BuscarImagem(Imagem model);
        UsuarioModel AtualizaClienteAtivo(UsuarioModel model);
        UsuarioModel AtualizaDados(UsuarioModel model);
        UsuarioModel AtualizaSenhaCliente(UsuarioModel model);
        UsuarioModel BuscaClientePorCPF(UsuarioModel model);
        UsuarioModel BuscaClientePorTelefone(UsuarioModel model);
        UsuarioModel ValidaUsuarioSenha(UsuarioModel model);
        UsuarioModel RegistraTokenAutenticacao(UsuarioModel model);
        bool BuscaSlug(string slug);

        //Task<bool> GravarTransacao(Dados model, ChargeResponseMessage request);



    }
}
