﻿using App.Model.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio.Interface
{
    public interface IRepositorioHanking
    {

        List<HankingModel> BuscarHankingVendas();
        List<HankingModel> BuscarHankingVendasIndicados();
        List<HankingModel> BuscarHankingDestaqueMes();




    }
}
