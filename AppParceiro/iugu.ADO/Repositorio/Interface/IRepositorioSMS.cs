﻿using App.Model.SMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio.Interface
{
    interface IRepositorioSMS
    {
        int GravarSMS(SMS model);
    }
}
