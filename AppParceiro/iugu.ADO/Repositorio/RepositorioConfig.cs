﻿using App.ADO.SqlServer;
using App.Model.Usuario;
using EZ.EZControl.ADO.Repositorio.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio
{
    public class RepositorioConfig : ConexaoSqlServer<ConfigModel>, IRepositorioConfig, ISqlServerInstanciavel<ConfigModel>
    {

        public RepositorioConfig(String connString) : base(connString) { }



        public List<ConfigModel> BuscaConfig(ConfigModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();
            List<ConfigModel> lista = new List<ConfigModel>();

            sqlQuery.AppendLine("SELECT  id ");
            sqlQuery.AppendLine(",email ");
            sqlQuery.AppendLine(",tipo ");
            sqlQuery.AppendLine(",ativo");
            sqlQuery.AppendLine(" FROM [Parceiros].[dbo].[configuracao_tb]  ");
            sqlQuery.AppendFormat("WHERE email = '{0}' ", model.email);

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                lista = Resultados;
            else
                lista = null;

            return lista;
        }


        public bool AtualizaConfig(ConfigRet model)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Clear();

            try
            {
                foreach (var item in model.config)
                {
                    sqlQuery.AppendFormat(" update [dbo].[configuracao_tb] set [Ativo] = '{0}' where 1=1 and email = '{1}' and tipo = '{2}' ", item.Ativo , model.email, item.tipo);
                }

                SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

                return ExecuteNonQuery(ref sqlCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }






        public ConfigModel Instanciar(SqlDataReader reader)
        {
            var dados = new  ConfigModel();
            
            dados.email = (reader["email"] != DBNull.Value) ? reader["email"].ToString() : null;

            dados.config = new config();
            dados.config.id = (reader["id"] != DBNull.Value) ? Convert.ToInt32(reader["id"]) : 0;
            dados.config.Ativo = (reader["Ativo"] != DBNull.Value) ? reader["Ativo"].ToString().Trim() : null;
            dados.config.tipo = (reader["tipo"] != DBNull.Value) ? reader["tipo"].ToString() : null;

            return dados;
        }
    }
}
