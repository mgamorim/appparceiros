﻿using App.ADO.SqlServer;
using App.Model.Usuario;
using EZ.EZControl.ADO.Repositorio.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio
{
    public class RepositorioTreinamento : ConexaoSqlServer<TreinamentoModel>, IRepositorioTreinamento, ISqlServerInstanciavel<TreinamentoModel>
    {
        public RepositorioTreinamento(String connString) : base(connString) { }


        public List<TreinamentoModel> BuscaTreinamento(UsuarioModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();
            List<TreinamentoModel> lista = new List<TreinamentoModel>();

            sqlQuery.AppendLine("SELECT  id ");
            sqlQuery.AppendLine(",titulo ");
            sqlQuery.AppendLine(",tipo ");
            sqlQuery.AppendLine(",conteudo");
            sqlQuery.AppendLine(" FROM [Parceiros].[dbo].[treinamento_tb] with(nolock) ");

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                lista = Resultados;
            else
                lista = null;

            return lista;

        }

        public TreinamentoModel Instanciar(SqlDataReader reader)
        {
            var dados = new TreinamentoModel();

            dados.id = (reader["id"] != DBNull.Value) ? Convert.ToInt32(reader["id"]) : 0;
            dados.Titulo = (reader["Titulo"] != DBNull.Value) ? reader["Titulo"].ToString().Trim() : null;
            dados.Tipo = (reader["Tipo"] != DBNull.Value) ? reader["Tipo"].ToString().Trim() : null;
            dados.Conteudo = (reader["Conteudo"] != DBNull.Value) ? reader["Conteudo"].ToString().Trim() : null;

            return dados;
        }
    }
}
