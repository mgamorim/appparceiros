﻿using App.ADO.SqlServer;
using App.Model.Usuario;
using EZ.EZControl.ADO.Repositorio.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio
{
    public class RepositorioDashboard : ConexaoSqlServer<DashboardModel>, IRepositorioDashboard, ISqlServerInstanciavel<DashboardModel>
    {
        public RepositorioDashboard(String connString) : base(connString) { }
        private string controle = string.Empty;


        public DashboardModel BuscaDashboard(UsuarioModel usuario)
        {

            DashboardModel model = new DashboardModel();
            StringBuilder sqlQuery = new StringBuilder();



            sqlQuery.Clear();
            sqlQuery.AppendFormat("select slug from [dbo].[Usuario_tb] where indicador = '{0}' ", usuario.Slug);
            SqlCommand sqlCommandPUsuario = new SqlCommand(sqlQuery.ToString());
            ConnString = ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString;

            var retornoSlugs = ExecuteReaderList(sqlCommandPUsuario, this, "slug");

            int virgula1 = retornoSlugs.Count > 1 ? retornoSlugs.Count - 1 : retornoSlugs.Count;

            string ListSlug = string.Empty;
            for (int i = 0; i < retornoSlugs.Count; i++)
            {
                ListSlug += retornoSlugs[i];
                ListSlug += virgula1 != retornoSlugs.Count ? "," : "";
                virgula1++;
            }



            sqlQuery.Clear();
            sqlQuery.AppendFormat("SELECT 'QtdSimulacao' = (select  count(*)  from Simulacao.dbo.Simulacao_tb with(nolock) where parceiro = '{0}' and MONTH(datasimulacao) = MONTH(GETDATE()) ), ", usuario.Slug);
            sqlQuery.AppendFormat(" 'QtdPropostas' = (select count(*) from Simulacao.dbo.Simulacao_tb with(nolock) where parceiro = '{0}' and MONTH(datasimulacao) = MONTH(GETDATE()) and idproposta is not null and idproposta <> '') , ", usuario.Slug);
            sqlQuery.AppendFormat(" 'QtdPropostasIndicados' = (select count(*) from Simulacao.dbo.Simulacao_tb with(nolock) where parceiro in ('{0}') and MONTH(datasimulacao) = MONTH(GETDATE()) and idproposta is not null and idproposta <> '')  ", ListSlug);

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            ConnString = ConfigurationManager.ConnectionStrings["SimuladorDB"].ConnectionString;
            controle = "simulador";

            var retorno = ExecuteReader(sqlCommand, this);

            if (retorno)
                model = Resultado;
            else
                model = null;


            
            sqlQuery.Clear();
            sqlQuery.AppendFormat("select idproposta from Simulacao.dbo.Simulacao_tb with(nolock)   where 1=1 and  parceiro = '{0}' and idproposta is not null  and idproposta <> '' ", usuario.Slug);
            SqlCommand sqlCommandSimulacao = new SqlCommand(sqlQuery.ToString());
            ConnString = ConfigurationManager.ConnectionStrings["SimuladorDB"].ConnectionString;

            var retornoQtdPropostasParceiro = ExecuteReaderList(sqlCommandSimulacao, this, "idproposta");

            int virgula = retornoQtdPropostasParceiro.Count > 1 ? retornoQtdPropostasParceiro.Count - 1 : retornoQtdPropostasParceiro.Count;

            string Listpropostas = string.Empty;
            for (int i = 0; i < retornoQtdPropostasParceiro.Count; i++)
            {
                Listpropostas += retornoQtdPropostasParceiro[i];
                Listpropostas += virgula != retornoQtdPropostasParceiro.Count ? "," : "";
                virgula++;
            }







            sqlQuery.Clear();
            sqlQuery.AppendFormat("select idproposta from Simulacao.dbo.Simulacao_tb with(nolock)   where 1=1 and  parceiro in ('{0}') and idproposta is not null  and idproposta <> '' ", ListSlug);
            SqlCommand sqlCommandListPropostasParceiro = new SqlCommand(sqlQuery.ToString());
            ConnString = ConfigurationManager.ConnectionStrings["SimuladorDB"].ConnectionString;

            var retornoPropostasIndicados = ExecuteReaderList(sqlCommandListPropostasParceiro, this, "idproposta");

            int virgula02 = retornoPropostasIndicados.Count > 1 ? retornoPropostasIndicados.Count - 1 : retornoPropostasIndicados.Count;

            string ListpropostasIndicados = string.Empty;
            for (int i = 0; i < retornoPropostasIndicados.Count; i++)
            {
                ListpropostasIndicados += retornoPropostasIndicados[i];
                ListpropostasIndicados += virgula02 != retornoPropostasIndicados.Count ? "," : "";
                virgula02++;
            }




            sqlQuery.Clear();

            sqlQuery.AppendFormat("SELECT 'valor' = (select convert(Numeric(10,2),ISNULL(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*40/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and status = 'Pago') +  ", Listpropostas == "" ? "0" : Listpropostas);
            sqlQuery.AppendFormat(" (select convert(Numeric(10,2),ISNULL(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*10/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and statusParceiro = 'Pago'), ", ListpropostasIndicados == "" ? "0" : ListpropostasIndicados);


            sqlQuery.AppendFormat("'ValorAReceber' =  (select convert(Numeric(10,2),ISNULL(SUM(convert(Numeric(10,2), REPLACE(VALOR,',','.'))),0)*40/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and status = 'Pendente'),", Listpropostas == "" ? "0" : Listpropostas);
            sqlQuery.AppendFormat("'QtdPropostasValorAReceber' = (select count(*) FROM[dbo].[Transacao_tb] where idproposta in ({0}) and status = 'Pendente'), ", Listpropostas == "" ? "0" : Listpropostas);
            sqlQuery.AppendFormat(" 'ValorAReceberIndicados' =  (select convert(Numeric(10,2),ISNULL(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*10/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and statusParceiro = 'Pendente'), ", ListpropostasIndicados == "" ? "0" : ListpropostasIndicados);
            sqlQuery.AppendFormat(" 'ValorAReceberIndicadosQtd' =  (select count(*) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and statusParceiro = 'Pendente'), ", ListpropostasIndicados == "" ? "0" : ListpropostasIndicados);
            sqlQuery.AppendFormat(" 'PendentePgtoIndicados' =  (select count(*) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and status = 'Pendente'), ", ListpropostasIndicados == "" ? "0" : ListpropostasIndicados);

            sqlQuery.AppendFormat(" 'Mes1' = (select convert(Numeric(10,2), isnull(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*40/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and status = 'Pago' and month(DataTransacao) = {1} ) + ", Listpropostas == "" ? "0" : Listpropostas, DateTime.Now.Month );
            sqlQuery.AppendFormat(" (select convert(Numeric(10,2), isnull(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*10/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and statusParceiro = 'Pago' and month(DataTransacao) = {1} ) , ", ListpropostasIndicados == "" ? "0" : ListpropostasIndicados, DateTime.Now.Month);


            sqlQuery.AppendFormat(" 'Mes2' = (select convert(Numeric(10,2), isnull(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*40/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and status = 'Pago' and month(DataTransacao) = {1} ) + ", Listpropostas == "" ? "0" : Listpropostas, DateTime.Now.Month-1);
            sqlQuery.AppendFormat(" (select convert(Numeric(10,2), isnull(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*10/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and statusParceiro = 'Pago' and month(DataTransacao) = {1} ) , ", ListpropostasIndicados == "" ? "0" : ListpropostasIndicados, DateTime.Now.Month-1);


            sqlQuery.AppendFormat(" 'Mes3' = (select convert(Numeric(10,2), isnull(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*40/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and status = 'Pago' and month(DataTransacao) = {1} ) + ", Listpropostas == "" ? "0" : Listpropostas, DateTime.Now.Month - 2);
            sqlQuery.AppendFormat(" (select convert(Numeric(10,2), isnull(sum(convert(Numeric(10,2), replace(Valor,',','.'))),0)*10/100) FROM [dbo].[Transacao_tb] where idproposta in ({0}) and statusParceiro = 'Pago' and month(DataTransacao) = {1} )  ", ListpropostasIndicados == "" ? "0" : ListpropostasIndicados, DateTime.Now.Month - 2);



            SqlCommand sqlCommandPagamento = new SqlCommand(sqlQuery.ToString());
            ConnString = ConfigurationManager.ConnectionStrings["PagamentoDB"].ConnectionString;
            controle = "pagamento";
            var retornoPagamento = ExecuteReader(sqlCommandPagamento, this);
            model.FaturamentoTotal = Resultado.FaturamentoTotal;
            model.ValorAReceber = Resultado.ValorAReceber;
            model.PendentePgto = Resultado.PendentePgto;
            model.ValorAReceberIndicados = Resultado.ValorAReceberIndicados;
            model.PendentePgtoIndicados = Resultado.PendentePgtoIndicados;
            model.Ultimos3meses = Resultado.Ultimos3meses;




            sqlQuery.Clear();
            sqlQuery.AppendFormat("select qtdindicados = (select count(*) as qtdindicados from [dbo].[Usuario_tb] where Indicador = '{0}')", usuario.Slug);
            sqlQuery.AppendFormat(", ContAcessos = (select ContAcessos from [dbo].[Usuario_tb] where slug = '{0}')", usuario.Slug);
            SqlCommand sqlCommandParceiro = new SqlCommand(sqlQuery.ToString());
            ConnString = ConfigurationManager.ConnectionStrings["ParceiroDB"].ConnectionString;
            controle = "parceiro";
            var retornoParceiro = ExecuteReader(sqlCommandParceiro, this);
            model.QtdIndicados = Resultado.QtdIndicados;
            model.QtdAcessosLink = Resultado.QtdAcessosLink;





            sqlQuery.Clear();
            sqlQuery.AppendFormat("SELECT 'QtdPropostasPendentes' = (select count(*)  FROM[dbo].[Sau_PropostaDeContratacao] where IsDeleted = 0 and PassoDaProposta < 60 and id in ({0})), ", Listpropostas == "" ? "0" : Listpropostas);
            sqlQuery.AppendFormat("'GerandoFatura' = (select count(*)  FROM[dbo].[Sau_PropostaDeContratacao] where IsDeleted = 0 and pedidoid is null and PassoDaProposta = 70 and StatusDaProposta<> 6 and id in ({0})), ", Listpropostas == "" ? "0" : Listpropostas);
            sqlQuery.AppendFormat("'QtdEmHomologacao' = (select count(*)  FROM[dbo].[Sau_PropostaDeContratacao] where IsDeleted = 0 and PassoDaProposta = 60 and StatusDaProposta<> 6 and id in ({0})), ", Listpropostas == "" ? "0" : Listpropostas);
            sqlQuery.AppendFormat("'Rejeitadas' = (select count(*) FROM[dbo].[Sau_PropostaDeContratacao] where IsDeleted = 0 and TipoDeHomologacao = 3 and StatusDaProposta = 6 and id in ({0})) ", Listpropostas == "" ? "0" : Listpropostas);

            SqlCommand sqlCommandPortal = new SqlCommand(sqlQuery.ToString());
            ConnString = ConfigurationManager.ConnectionStrings["PortalDB"].ConnectionString;
            controle = "portal";
            var retornoPortal = ExecuteReader(sqlCommandPortal, this);


            model.QtdPropostasPendentes = Resultado.QtdPropostasPendentes;
            model.QtdEmHomologacao = Resultado.QtdEmHomologacao;
            model.Rejeitadas = Resultado.Rejeitadas;
            model.GerandoFatura = Resultado.GerandoFatura;



            return model;
        }

        public DashboardModel Instanciar(SqlDataReader reader)
        {
            var dados = new DashboardModel();

            try
            {
                dados.ValorAReceberIndicados = new ValorAReceberIndicados();

                if (controle == "simulador")
                {
                    dados.QtdSimulacao = (reader["QtdSimulacao"] != DBNull.Value) ? reader["QtdSimulacao"].ToString() : dados.QtdSimulacao;
                    dados.QtdPropostas = (reader["QtdPropostas"] != DBNull.Value) ? reader["QtdPropostas"].ToString() : dados.QtdPropostas;
                    dados.QtdPropostasIndicados = (reader["QtdPropostasIndicados"] != DBNull.Value) ? reader["QtdPropostasIndicados"].ToString() : dados.QtdPropostasIndicados;
                }

                if (controle == "pagamento")
                {
                    dados.FaturamentoTotal = (reader["valor"] != DBNull.Value) ? reader["valor"].ToString() : dados.FaturamentoTotal;
                    dados.ValorAReceber = new ValorAReceber();
                    dados.ValorAReceber.Valor = (reader["ValorAReceber"] != DBNull.Value) ? reader["ValorAReceber"].ToString() : dados.ValorAReceber.Valor;
                    dados.ValorAReceber.QtdPropostasValorAReceber = (reader["QtdPropostasValorAReceber"] != DBNull.Value) ? reader["QtdPropostasValorAReceber"].ToString() : dados.ValorAReceber.QtdPropostasValorAReceber;
                    dados.PendentePgto = dados.ValorAReceber.QtdPropostasValorAReceber;

                    dados.ValorAReceberIndicados.Valor = (reader["ValorAReceberIndicados"] != DBNull.Value) ? reader["ValorAReceberIndicados"].ToString() : dados.ValorAReceberIndicados.Valor;
                    dados.ValorAReceberIndicados.QtdPropostasValor = (reader["ValorAReceberIndicadosQtd"] != DBNull.Value) ? reader["ValorAReceberIndicadosQtd"].ToString() : dados.ValorAReceberIndicados.QtdPropostasValor;
                    dados.PendentePgtoIndicados = (reader["PendentePgtoIndicados"] != DBNull.Value) ? reader["PendentePgtoIndicados"].ToString() : dados.PendentePgtoIndicados;


                    
                    dados.Ultimos3meses = new List<Ultimos3meses>();
                    dados.Ultimos3meses.Add(new Ultimos3meses { Nome =  TraduzMes(DateTime.Now.Month) , Ordem = "01", Valor = (reader["Mes1"] != DBNull.Value) ? reader["Mes1"].ToString() : "0" });
                    dados.Ultimos3meses.Add(new Ultimos3meses { Nome =  TraduzMes(DateTime.Now.Month-1)  , Ordem = "02", Valor =  (reader["Mes2"] != DBNull.Value) ? reader["Mes2"].ToString() : "0" });
                    dados.Ultimos3meses.Add(new Ultimos3meses { Nome =  TraduzMes(DateTime.Now.Month-2)  , Ordem = "03", Valor =  (reader["Mes3"] != DBNull.Value) ? reader["Mes3"].ToString() : "0" });
                }

                if (controle == "portal")
                {
                    dados.GerandoFatura = (reader["GerandoFatura"] != DBNull.Value) ? reader["GerandoFatura"].ToString() : dados.GerandoFatura;
                    dados.QtdPropostasPendentes = (reader["QtdPropostasPendentes"] != DBNull.Value) ? reader["QtdPropostasPendentes"].ToString() : dados.QtdPropostasPendentes;
                    dados.QtdEmHomologacao = (reader["QtdEmHomologacao"] != DBNull.Value) ? reader["QtdEmHomologacao"].ToString() : dados.QtdEmHomologacao;
                    dados.Rejeitadas = (reader["Rejeitadas"] != DBNull.Value) ? reader["Rejeitadas"].ToString() : dados.Rejeitadas;
                }


                if (controle == "parceiro")
                {
                    dados.QtdIndicados = (reader["QtdIndicados"] != DBNull.Value) ? reader["QtdIndicados"].ToString() : dados.QtdIndicados;
                    dados.QtdAcessosLink = (reader["ContAcessos"] != DBNull.Value) ? reader["ContAcessos"].ToString() : dados.QtdIndicados;
                }

            }
            catch (Exception exp)
            {

            }
            return dados;

        }


        private string TraduzMes(int mesAtual)
        {
            string mes = string.Empty;

            if (mesAtual == 1)
            {
                mes = "Janeiro";
            }
            if (mesAtual == 2)
            {
                mes = "Fevereiro";
            }
            if (mesAtual == 3)
            {
                mes = "Março";
            }
            if (mesAtual == 4)
            {
                mes = "Abril";
            }
            if (mesAtual == 5)
            {
                mes = "Maio";
            }
            if (mesAtual == 6)
            {
                mes = "Junho";
            }
            if (mesAtual == 7)
            {
                mes = "Julho";
            }
            if (mesAtual == 8)
            {
                mes = "Agosto";
            }
            if (mesAtual == 9)
            {
                mes = "Setembro";
            }
            if (mesAtual == 10)
            {
                mes = "Outubro";
            }
            if (mesAtual == 11 || mesAtual == -1)
            {
                mes = "Novembro";
            }
            if (mesAtual == 12 || mesAtual == 0)
            {
                mes = "Dezembro";
            }

            return mes;
        }

    }
}
