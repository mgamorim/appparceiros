﻿using App.ADO.Enum;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace App.ADO.Mensageria
{
    [Serializable]
    public class MsgResposta
    {
        #region Atributos

        private bool sucesso;
        private int codigo;
        private string descricao;
        private List<Mensagem> mensagens;

        private Stopwatch stopwatch;
        private long tempoProcessamento;
        private string emissor;
        private string acao;
        private TipoEnum tipo;
        private MsgResposta msgRespostaInterna;

        #endregion

        #region Construtores

        public MsgResposta(bool iniciarCronometro = false)
            : this(string.Empty, string.Empty, TipoEnum.Indefinido, iniciarCronometro)
        {
        }

        public MsgResposta(string emissor, string acao, TipoEnum tipo, bool iniciarCronometro = false)
        {
            this.sucesso = false;
            this.codigo = 0;
            this.descricao = string.Empty;
            this.mensagens = new List<Mensagem>();
            this.stopwatch = new Stopwatch();
            this.tempoProcessamento = 0L;
            this.emissor = emissor;
            this.acao = acao;
            this.tipo = tipo;
            this.msgRespostaInterna = null;

            if (iniciarCronometro)
            {
                this.stopwatch.Start();
            }
        }

        #endregion

        #region Propriedades

        [DataMember]
        public Stopwatch Stopwatch
        {
            get { return this.stopwatch; }
        }

        [DataMember]
        public bool Sucesso
        {
            get { return sucesso; }
            set { sucesso = value; }
        }

        [DataMember]
        public int Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        [DataMember]
        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        [DataMember]
        public List<Mensagem> Mensagens
        {
            get { return mensagens; }
            set { mensagens = value; }
        }

        [DataMember]
        public long TempoProcessamento
        {
            get { return tempoProcessamento; }
        }

        [DataMember]
        public string Emissor
        {
            get { return emissor; }
            set { emissor = value; }
        }

        [DataMember]
        public string Acao
        {
            get { return acao; }
            set { acao = value; }
        }

        [DataMember]
        public TipoEnum Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        [DataMember]
        public MsgResposta MsgRespostaInterna
        {
            get { return msgRespostaInterna; }
            set { msgRespostaInterna = value; }
        }

        #endregion

        #region Operacoes

        public void IniciarCronometro()
        {
            stopwatch.Start();
        }

        public void PararCronometro()
        {
            stopwatch.Stop();
            tempoProcessamento = stopwatch.ElapsedMilliseconds;
        }

        public void Positivar()
        {
            sucesso = true;
            PararCronometro();
        }

        public void Negativar()
        {
            sucesso = false;
            PararCronometro();
        }

        public void Negativar(int codigo, string descricao)
        {
            this.sucesso = false;
            this.codigo = codigo;
            this.descricao = descricao;

            PararCronometro();
        }

        public void Negativar(int codigo, string descricao, Mensagem mensagem)
        {
            Negativar(codigo, descricao);
            this.mensagens.Add(mensagem);
        }

        public void Negativar(int codigo, string descricao, List<Mensagem> mensagens)
        {
            Negativar(codigo, descricao);
            this.mensagens = mensagens;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("MsgResposta[ ");

            sb.Append("Sucesso: "); sb.Append(sucesso);
            sb.Append(" | ");
            sb.Append("Código: "); sb.Append(codigo);
            sb.Append(" | ");
            sb.Append("Descrição: "); if (!string.IsNullOrWhiteSpace(descricao)) sb.Append(descricao);
            sb.Append(" | ");
            sb.Append("Tempo de processamento (ms):"); sb.Append(tempoProcessamento);
            sb.Append(" | ");
            sb.Append("Emissor:"); sb.Append(emissor);
            sb.Append(" | ");
            sb.Append("Ação:"); sb.Append(acao);
            sb.Append(" | ");
            sb.Append("Tipo:"); sb.Append(tipo);
            sb.Append(" | ");

            if (mensagens.Count > 0)
            {
                sb.Append(" | ");
                sb.Append("Mensagens: ");

                foreach (Mensagem mensagem in mensagens)
                {
                    sb.Append(mensagem);
                    sb.Append(" | ");
                }
            }

            sb.Append("]");

            return sb.ToString();
        }

        #endregion
    }

    [Serializable]
    public class MsgResposta<T> : MsgResposta where T : class
    {
        #region Atributos

        private int contagem;
        private List<T> objetos;

        #endregion

        #region Construtores

        public MsgResposta(bool iniciarCronometro = false)
            : base(iniciarCronometro)
        {
            this.contagem = 0;
            this.objetos = new List<T>();
        }

        public MsgResposta(string emissor, string acao, TipoEnum tipo, bool iniciarCronometro = false) : base(emissor, acao, tipo, iniciarCronometro)
        {
            this.contagem = 0;
            this.objetos = new List<T>();
        }

        #endregion

        #region Propriedades

        [DataMember]
        public int Contagem
        {
            get { return contagem; }
            set { contagem = value; }
        }

        [DataMember]
        public bool PossuiObjetos
        {
            get { return objetos.Count > 0; }
        }

        [DataMember]
        public T Objeto
        {
            get
            {
                if (objetos.Count > 0)
                {
                    return objetos[0];
                }
                else
                {
                    return default(T);
                }
            }
        }

        [DataMember()]
        public List<T> Objetos
        {
            get { return objetos; }
            set { objetos = value; }
        }

        #endregion

        #region Operacoes

        public void Positivar(T objeto)
        {
            Positivar(new List<T>() { objeto });
        }

        public void Positivar(List<T> objetos)
        {
            base.Sucesso = true;
            this.objetos = objetos;
            base.PararCronometro();
        }
        //
        public void Negativar(T objeto)
        {
            base.Negativar();
            objetos = new List<T>() { objeto };
        }

        public void Negativar(T objeto, int codigo, string descricao)
        {
            base.Negativar(codigo, descricao);
            objetos = new List<T>() { objeto };
        }

        public void Negativar(T objeto, int codigo, string descricao, Mensagem mensagem)
        {
            base.Negativar(codigo, descricao, mensagem);
            objetos = new List<T>() { objeto };
        }

        public void Negativar(T objeto, int codigo, string descricao, List<Mensagem> mensagens)
        {
            base.Negativar(codigo, descricao, mensagens);
            objetos = new List<T>() { objeto };
        }

        #endregion
    }

}
