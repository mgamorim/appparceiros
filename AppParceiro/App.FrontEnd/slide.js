
function fnSlideRight()
{
    if(location.hash == '#/divulgacao'){
        let slideMenuRight = null;    

        let whidth_height = window.fnCurrentScreenSize();
        
        let ctn = document.getElementById("fab_ctn_open");
        if(whidth_height.w < 400 ){
            slideMenuRight = new OSREC.superslide(
            {
                slider: 					document.getElementById('menuRight'),
                content: 					document.getElementById('content'),
                animation: 					'slideRight',
                duration: 					0.5,
                allowDrag: 					true,
                slideContent: 				false,
                allowContentInteraction:	false,
                closeOnBlur:				false,
                width: 						'90vw',
                height:						'50vh'
            });
            ctn.style.right = '1vw';
            ctn.style.bottom = '45vw'; 
        }else if(whidth_height.w < 810){
            slideMenuRight = new OSREC.superslide(
            {
                slider: 					document.getElementById('menuRight'),
                content: 					document.getElementById('content'),
                animation: 					'slideRight',
                duration: 					0.5,
                allowDrag: 					true,
                slideContent: 				false,
                allowContentInteraction:	false,
                closeOnBlur:				false,
                width: 						'60vw',
                height:						'50vh'
            });        
            ctn.style.right = '1vw';
            ctn.style.bottom = '20vw'; 
        }else{
            slideMenuRight = new OSREC.superslide(
            {
                slider: 					document.getElementById('menuRight'),
                content: 					document.getElementById('content'),
                animation: 					'slideRight',
                duration: 					0.5,
                allowDrag: 					true,
                slideContent: 				false,
                allowContentInteraction:	false,
                closeOnBlur:				false,
                width: 						'60vw',
                height:						'50vh'
            });       
            ctn.style.right = '1vw';
            ctn.style.bottom = '27vw'; 
        }      

        window.MENUright = slideMenuRight;          

        if(!window.Material_Stepper){
            let stepper_Element = document.querySelector('.mdl-stepper#demo-stepper-nonlinear');
            let _Stepper = stepper_Element.MaterialStepper;
            window.Material_Stepper = _Stepper;
        }
    }else{
        return false;
    }
};

function fnSlideBottom() {
    let whidth_height = window.fnCurrentScreenSize();
        if(whidth_height.h < 674 ){
                slideMenuBottom = new OSREC.superslide(
                    {
                        slider: 					document.getElementById('menuBottom'),
                        content: 					document.getElementById('content'),
                        animation: 					'slideBottom',
                        duration: 					0.7,
                        allowDrag: 					false,
                        slideContent: 				false,
                        allowContentInteraction:	true,
                        closeOnBlur:				true,
                        width: 						'60vw',
                        height:						'92vh'
                    });
                window.MENUbottom = slideMenuBottom;               
         }else if(whidth_height.h < 701 ){             
                slideMenuBottom = new OSREC.superslide(
                    {
                        slider: 					document.getElementById('menuBottom'),
                        content: 					document.getElementById('content'),
                        animation: 					'slideBottom',
                        duration: 					0.7,
                        allowDrag: 					false,
                        slideContent: 				false,
                        allowContentInteraction:	true,
                        closeOnBlur:				true,
                        width: 						'60vw',
                        height:						'91vh'
                    });
                window.MENUbottom = slideMenuBottom;               
         }else{
                slideMenuBottom = new OSREC.superslide(
                    {
                        slider: 					document.getElementById('menuBottom'),
                        content: 					document.getElementById('content'),
                        animation: 					'slideBottom',
                        duration: 					0.7,
                        allowDrag: 					false,
                        slideContent: 				false,
                        allowContentInteraction:	true,
                        closeOnBlur:				true,
                        width: 						'60vw',
                        height:						'94vh'
                    });
                window.MENUbottom = slideMenuBottom;               
         }
}

function currentScreenSize(){
    var x = document.body.scrollWidth || document.body.offsetWidth;
    var y = document.body.scrollHeight || document.body.offsetHeight;
    return { w: x, h: y };
}


function fnShareDropdown(elementNode, option){
    return new needShareDropdown(elementNode, option);
}

window.fnSuperSlideRight = fnSlideRight;

window.fnSuperSlideBottom = fnSlideBottom;

//window.onload = fnSlideRight;

window.fnNeedShareDropdown = fnShareDropdown;

window.fnCurrentScreenSize = currentScreenSize;