﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model.SMS
{
  public  class SMS
    {
        public string Numero { get; set; }
        public string Mensagem { get; set; }
        public string Titulo { get; set; }
        public string Senha { get; set; }
        public string Id { get; set; }

    }
}
