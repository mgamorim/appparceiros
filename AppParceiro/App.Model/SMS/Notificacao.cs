﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model.SMS
{
    public class Notificacao
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Descricao { get; set; }
        public string Links { get; set; }
        public string Lida { get; set; }
        public string Tipo { get; set; }
        public string Data { get; set; }
    }
}
