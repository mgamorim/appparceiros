﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model.Usuario
{
    public class HankingModel
    {
        public int Posicao { get; set; }
        public string Nick { get; set; }
        public decimal TotalComissao { get; set; }
        public Int32 TotalIndicados { get; set; }
    }
}
