﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model.Usuario
{
   public class DashboardModel
    {

        public int IdCliente { get; set; }
        public string Email { get; set; }

        
        public string FaturamentoTotal { get; set; }
        public ValorAReceber ValorAReceber { get; set; } // valor e qtd propostas
        public ValorAReceberIndicados ValorAReceberIndicados { get; set; } // valor e qtd propostas


        public string QtdAcessosLink { get; set; }
        public string QtdSimulacao { get; set; }
        public string QtdPropostas { get; set; }
        public string QtdPropostasPendentes { get; set; }
        public string GerandoFatura { get; set; }
        public string QtdEmHomologacao { get; set; }
        public string Rejeitadas { get; set; }
        public string PendentePgto { get; set; }



        public string QtdIndicados { get; set; }
        public string QtdPropostasIndicados { get; set; }
        public string PendentePgtoIndicados { get; set; }
        

        public List<Ultimos3meses> Ultimos3meses { get; set; }
    }


    public class Ultimos3meses
    {
        public string Ordem { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
    }


    public class ValorAReceber
    {
        public string Valor { get; set; }
        public string QtdPropostasValorAReceber { get; set; }
    }

    public class ValorAReceberIndicados
    {
        public string Valor { get; set; }
        public string QtdPropostasValor { get; set; }
    }
}
