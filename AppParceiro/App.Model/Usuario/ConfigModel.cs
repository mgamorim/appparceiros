﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model.Usuario
{
    public class ConfigModel
    {
        public string email { get; set; }
        public string Erro { get; set; }
        public config config { get; set; }

    }


    public class config
    {
        public int id { get; set; }
        public string tipo { get; set; }
        public string Ativo { get; set; }
    }



    public class ConfigRet
    {
        public string email { get; set; }
        public string Erro { get; set; }
        public List<config> config { get; set; }

    }




}
