﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model.Usuario
{
   public  class DadosBancarios
    {
        public string NomeBanco { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string TipoConta { get; set; }
        public string NomeFavorecido { get; set; }
        public string Cpf { get; set; }
    }
}
