﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model.Usuario
{
    public class DadosDivulgacao
    {

        public int IdCliente { get; set; }
        public string LinkVendas { get; set; }
        public string ImgBanner1 { get; set; }
        public string ImgBanner2 { get; set; }
        public string ImgBanner3 { get; set; }
        public string QrCode { get; set; }
        public string QrCodeCadastro { get; set; }
        public string LinkVideo1 { get; set; }
        public string LinkCadastroIndicador { get; set; }
        public string ImgBannerIndicador { get; set; }
        public List<Texto> texto { get; set; }

    }




    public class Texto
    {
        public string Tipo { get; set; }
        public string texto { get; set; }
    }


}
