﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model.Usuario
{
  public  class UsuarioModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string CPF { get; set; }
        public string DataNascimento { get; set; }
        public string Slug { get; set; }
        public string Imagem { get; set; }
        public string Indicador { get; set; }


        public string Login { get; set; }
        public string Senha { get; set; }
        public string ConfirmarSenha { get; set; }
        public string Token { get; set; }
        public string ConfirmaToken { get; set; }
        public int Ativo { get; set; }



        public string TokenAutenticacao { get; set; }
        public string Erro { get; set; }
        public string Sucess { get; set; }


        public DadosBancarios dadosBancarios { get; set; }
        public DadosEndereco dadosEndereco { get; set; }

    }
}
