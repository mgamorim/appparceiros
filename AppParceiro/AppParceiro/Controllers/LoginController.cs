﻿using App.Model.Usuario;
using App.Servicos.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;

namespace AppParceiro.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public JsonResult Login(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicao(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                   var retorno = servico.ValidaUsuarioSenha(model);

                    if (retorno != null)
                    {
                       var Encode = Convert.ToBase64String(Encoding.UTF8.GetBytes(retorno.Id + ":" + model.Senha));
                       //var Decode =  Encoding.UTF8.GetString(Convert.FromBase64String(Encode));

                        model.Id = retorno.Id;
                        model.TokenAutenticacao = Encode;
                        model.Email = model.Login;

                        var retornoHistorico = servico.RegistraTokenAutenticacao(model);

                        return Json(model.TokenAutenticacao);
                    }
                    else
                    {


                        return Json("Usuário ou senha Inválidos");
                    }
                }
                else
                {
                    return Json("Favor Preencher Login e Senha");
                }
                
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }






        private bool ValidarRequisicao(UsuarioModel model)
        {
            bool ret = false;

             if (model.Login == null)
                ret = false;
            else if (model.Senha == null)
                ret = false;
            else
                ret = true;

            return ret;
        }

    }
}