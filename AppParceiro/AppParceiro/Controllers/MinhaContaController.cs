﻿using App.Model.Usuario;
using App.Servicos.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppParceiro.Controllers
{
    public class MinhaContaController : Controller
    {
        // GET: MinhaConta
        public ActionResult Index()
        {
            return View();
        }



        [HttpPost]
        public JsonResult AtualizarDados(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicao(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {
                        Cliente.Nome = model.Nome;
                        Cliente.Telefone = model.Telefone;
                        Cliente.DataNascimento = model.DataNascimento;
                        Cliente.Senha = model.Senha;
                        Cliente.Imagem = model.Imagem;
                        Cliente.dadosBancarios.NomeBanco = model.dadosBancarios.NomeBanco;
                        Cliente.dadosBancarios.Agencia = model.dadosBancarios.Agencia;
                        Cliente.dadosBancarios.Conta = model.dadosBancarios.Conta;
                        Cliente.dadosBancarios.TipoConta = model.dadosBancarios.TipoConta;
                        Cliente.dadosBancarios.NomeFavorecido = model.dadosBancarios.NomeFavorecido;

                          servico.AtualizaDados(Cliente);

                        return Json("Atualizado com Sucesso");
                    }
                    else
                    {

                        return Json("Email não Cadastrado!");
                    }
                }
                else
                {
                    return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult BuscarDados(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicao(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {
                        Cliente.Senha = null;
                        Cliente.Token = null;
                        Cliente.dadosBancarios.Cpf = Cliente.CPF;

                        return Json(Cliente);
                    }
                    else
                    {

                        return Json("Email não Cadastrado!");
                    }
                }
                else
                {
                    return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

       

        private bool ValidarRequisicao(UsuarioModel model)
        {
            bool ret = false;
            ServicoUsuario servico = new ServicoUsuario();



             if (model.Email == null)
            {
                ret = false;
                model.Erro = "Email Invalido!";
            }
            else
            {
                ret = true;
            }

            return ret;
        }

    }
}