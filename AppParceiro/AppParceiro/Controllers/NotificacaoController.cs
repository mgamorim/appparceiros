﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Model.SMS;
using App.Servicos.SMS;

namespace AppParceiro.Controllers
{
    public class NotificacaoController : Controller
    {
        // GET: Notificacao
        public ActionResult Index()
        {
            return View();
        }



        [HttpPost]
        public JsonResult Gravar(Notificacao model)
        {
            try
            {
                ServicoNotificacao servico = new ServicoNotificacao();


                if (ValidarRequisicao(model))
                {
                    var result = servico.Gravar(model);

                    if(result == true)
                        return Json("Sucesso!");
                    else
                        return Json("Erro");
                }
                else
                {
                    return Json("Dados Informado Incorretos");
                }

            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult Buscar(Notificacao model)
        {
            try
            {
                ServicoNotificacao servico = new ServicoNotificacao();

                if (ValidarRequisicaoBuscar(model))
                {
                    var result = servico.Buscar(model);

                    if (result != null)
                    {
                        return Json(result);
                    }
                    else
                    {
                        return Json("sem dados");

                    }

                }
                else
                {
                    return Json("Favor Preencher Login e Senha");
                }

            }
            catch (Exception ex)
            {
                return Json("");
            }
        }







        private bool ValidarRequisicao(Notificacao model)
        {
            bool ret = false;

            if (model.Email == null)
                ret = false;
            else if (model.Descricao == null)
                ret = false;
            else if (model.Tipo == null)
                ret = false;
            else
                ret = true;

            return ret;
        }

        private bool ValidarRequisicaoBuscar(Notificacao model)
        {
            bool ret = false;

            if (model.Email == null)
                ret = false;
            else
                ret = true;

            return ret;
        }


    }
}