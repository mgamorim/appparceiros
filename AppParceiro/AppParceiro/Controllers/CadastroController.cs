﻿using App.Servicos.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using App.Model.Usuario;
using App.Model.SMS;
using App.Servicos.SMS;

namespace AppParceiro.Controllers
{
    public class CadastroController : Controller
    {
        // GET: Cadastro
        public ActionResult Index()
        {
            return View();
        }
        

        //[HttpPost]
        //public JsonResult Cadastrar(UsuarioModel model)
        //{
        //    try
        //    {
        //        if (ValidarRequisicao(model))
        //        {
        //            ServicoUsuario servico = new ServicoUsuario();

        //            var FormatNome = model.Nome.Split(' ');

        //            model.Token = GeraToken().ToString();
        //            model.Slug = FormatNome[0];

        //            var ExisteSlug = false;

        //            do
        //            {
        //                ExisteSlug = servico.BuscaSlug(model);

        //                if (ExisteSlug != false)
        //                {
        //                    int i = 1;
        //                    model.Slug = FormatNome[0] + i++;
        //                }
        //            } while (ExisteSlug);


        //           // var retorno = servico.CadastrarUsuario(model);

        //            /// ENVIAR TOKEN PARA O CLIENTE TokenAtual
        //            /// 
        //            ServicoSMS sms = new ServicoSMS();
        //            SMS dadosSMS = new SMS();

        //            dadosSMS.Mensagem = "Olá ! para Iniciar precisamos que confirme o Token de Acesso: " + model.Token;
        //            dadosSMS.Numero = model.Telefone;
        //            dadosSMS.Senha = "010203";
        //            dadosSMS.Titulo = "Evida Saúde Seguros";

        //            sms.EnviarSMS(dadosSMS);



        //            return Json(retorno);
        //        }
        //        else
        //        {
        //            return Json(model.Erro);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json("");
        //    }
        //}

        [HttpPost]
        public JsonResult ValidarToken(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicaoToken(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                       var ExisteSlug = servico.BuscarClientePorEmail(model);

                    if (ExisteSlug.Token == model.Token)
                    {

                        model.Ativo = 1;
                        model.ConfirmaToken = "S";
                        servico.AtualizaClienteAtivo(model);


                        ServicoSMS sms = new ServicoSMS();
                        SMS dadosSMS = new SMS();

                        dadosSMS.Mensagem = "Seja Bem vindo(a) " + ExisteSlug.Nome + "! Agora você é um parceiro de divulgação da Evida !";
                        dadosSMS.Numero = ExisteSlug.Telefone;
                        dadosSMS.Senha = "010203";
                        dadosSMS.Titulo = "Evida Saúde Seguros";

                        sms.EnviarSMS(dadosSMS);

                        dadosSMS.Mensagem = "Só precisa divulgar, nos fechamos a venda para você! indique um Parceiro, e ganhe vitalício em todas as vendas, Vamos começar?";
                        sms.EnviarSMS(dadosSMS);



                        return Json("Token Validado com Sucesso!");
                    }
                    else
                    {

                        return Json("Token Inválido!");
                    }
                }
                else
                {
                    return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult RecuperarSenha(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicaoToken(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {
                       model.Token = GeraToken().ToString();
                        model.Ativo = 0;
                        model.ConfirmaToken = "N";
                        servico.AtualizaClienteAtivo(model);

                        // disparo SMS COM o Token  para o cliente
                        ServicoSMS sms = new ServicoSMS();
                        SMS dados = new SMS();

                        dados.Numero = Cliente.Telefone;
                        dados.Mensagem = "Token de Redefinição de Senha: " + model.Token;
                        dados.Senha = "010203";
                        dados.Titulo = "Evida Saúde Seguros";


                        sms.EnviarSMS(dados);


                        return Json("Enviamos um SMS com o Token para o Telefone (**)****-" + Cliente.Telefone.Substring(7));
                    }
                    else
                    {

                        return Json("Erro Interno!");
                    }
                }
                else
                {
                    return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult CriarNovaSenha(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicaoNovaSenha(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                        servico.AtualizaSenhaCliente(model);

                        return Json("Senha Atualizada com Sucesso");
                }
                else
                {
                    return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }
                

        private int GeraToken()
        {
            int token = 0;

            var chars = "0123456789";
            var random = new Random();
            var TokenAtual = new string(Enumerable.Repeat(chars, 4).Select(s => s[random.Next(s.Length)]).ToArray());
            token = Convert.ToInt32(TokenAtual);


            return token;
        }

        private bool ValidarRequisicao(UsuarioModel model)
        {
            bool ret = false;
            ServicoUsuario servico = new ServicoUsuario();

            var ValidClienteEmail = servico.BuscarClientePorEmail(model);
            var ValidClienteCPF = servico.BuscarClientePorCPF(model);


            if (model.Nome == null)
            {
                ret = false;
                model.Erro = "Digite Nome de usuário!";
            }
            else if (model.Telefone == null)
            {
                ret = false;
                model.Erro = "Digite Numero de Telefone!";
            }
            else if (model.Email == null)
            {
                ret = false;
                model.Erro = "Digite o Email!";
            }
            else if (ValidClienteEmail != null)
            {
                ret = false;
                model.Erro = "Este Email já está cadastrado!";
            }
            else if (model.DataNascimento == null)
            {
                ret = false;
                model.Erro = "Digite a Data de Nascimento!";
            }
            else if (model.CPF == null)
            {
                ret = false;
                model.Erro = "Digite o CPF!";
            }
            else if (ValidClienteCPF != null)
            {
                ret = false;
                model.Erro = "Este CPF já está Possui uma Conta Associada!";
            }
            else if (model.Senha == null)
            {
                ret = false;
                model.Erro = "Informe a Senha!";
            }
            else
            {
                ret = true;
            }

            return ret;
        }

        private bool ValidarRequisicaoToken(UsuarioModel model)
        {
            bool ret = false;

            if (model.Email == null)
            {
                ret = false;
                model.Erro = "Digite o Email!";
            }
            else
            {
                ret = true;
            }

            return ret;
        }

        private bool ValidarRequisicaoNovaSenha(UsuarioModel model)
        {
            bool ret = false;

            if (model.Email == null)
            {
                ret = false;
                model.Erro = "Email do usuario não encontrado!";
            }
            if (model.Senha == null)
            {
                ret = false;
                model.Erro = "Digite a Senha!";
            }
            else if (model.ConfirmarSenha == null)
            {
                ret = false;
                model.Erro = "Confirme a Nova Senha!";
            }
            else if (model.ConfirmarSenha != model.Senha)
            {
                ret = false;
                model.Erro = "As Senhas não Coincidem!";
            }
            else
            {
                ret = true;
            }

            return ret;
        }

    }
}