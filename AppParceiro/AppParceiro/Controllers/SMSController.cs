﻿using App.Model.SMS;
using App.Servicos.SMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace AppParceiro.Controllers
{
    public class SMSController : ApiController
    {



        [HttpPost]
        public HttpResponseMessage EnviarSMS(SMS dados)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();

            try
            {
                if (dados.Mensagem == "")
                {
                     httpResponseMessage.StatusCode = HttpStatusCode.BadRequest;
                     return httpResponseMessage;
                }
                if (dados.Titulo == "")
                {
                    httpResponseMessage.StatusCode = HttpStatusCode.BadRequest;
                    return httpResponseMessage;
                }
                if (dados.Numero == "")
                {
                    httpResponseMessage.StatusCode = HttpStatusCode.BadRequest;
                    return httpResponseMessage;
                }
                if (dados.Senha == "" || dados.Senha != "010203")
                {
                    httpResponseMessage.StatusCode = HttpStatusCode.Unauthorized;
                    return httpResponseMessage;
                }


                ServicoSMS sms = new ServicoSMS();

               var retorno = sms.EnviarSMS(dados);

                if (retorno == true)
                    httpResponseMessage.StatusCode = HttpStatusCode.OK;
                else
                    httpResponseMessage.StatusCode = HttpStatusCode.InternalServerError;


                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return httpResponseMessage;
            }
        }








    }
}
