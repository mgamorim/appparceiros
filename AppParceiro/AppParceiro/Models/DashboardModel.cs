﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppParceiro.Models
{
    public class DashboardModel
    {
        public int IdCliente { get; set; }
        public string Faturamento { get; set; }

        public string Disparos { get; set; }
        public string Propostas { get; set; }
        public string Finalizadas { get; set; }
        public string Rejeitadas { get; set; }
        public string PendentePgto { get; set; }

    }
}