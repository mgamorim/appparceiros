﻿using App.Model.Usuario;
using App.Servicos.Usuario;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;

namespace App.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {
        // GET: Login
        //public ActionResult Index()
        //{
        //    return View();
        //}

        [Route("api/v1/Login/Login")]
        public HttpResponseMessage Login(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicao(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                   var retorno = servico.ValidaUsuarioSenha(model);

                    if (retorno != null)
                    {
                        var chars = "0123456789";
                        var random = new Random();
                        var ret = new string(Enumerable.Repeat(chars, 10).Select(s => s[random.Next(s.Length)]).ToArray());

                        var Encode = Convert.ToBase64String(Encoding.UTF8.GetBytes(ret));
                        //var Decode =  Encoding.UTF8.GetString(Convert.FromBase64String(Encode));

                        model.Id = retorno.Id;
                        model.TokenAutenticacao = Encode;
                        model.Email = model.Login;

                        var retornoHistorico = servico.RegistraTokenAutenticacao(model);

                        return Request.CreateResponse(HttpStatusCode.OK, model.TokenAutenticacao);
                        //return Json(model.TokenAutenticacao);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "Usuário ou senha Inválidos");
                        //return Json("Usuário ou senha Inválidos");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, "Favor Preencher Login e Senha");
                    //return Json("Favor Preencher Login e Senha");
                }
                
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }
        

        private bool ValidarRequisicao(UsuarioModel model)
        {
            bool ret = false;

             if (model.Login == null)
                ret = false;
            else if (model.Senha == null)
                ret = false;
            else
                ret = true;

            return ret;
        }

    }
}