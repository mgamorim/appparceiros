﻿using App.Model.SMS;
using App.Servicos.SMS;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace App.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NotificacaoController : ApiController
    {
        // GET: Notificacao
        //public ActionResult Index()
        //{
        //    return View();
        //}

        [Route("api/v1/Notificacao/Gravar")]
        public HttpResponseMessage Gravar(Notificacao model)
        {
            try
            {
                ServicoNotificacao servico = new ServicoNotificacao();


                if (ValidarRequisicao(model))
                {
                    var result = servico.Gravar(model);

                    if(result == true)
                        return Request.CreateResponse<Notificacao>(HttpStatusCode.OK, model);
                    else
                        throw new HttpResponseException(HttpStatusCode.NotFound);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, "Dados Informado Incorretos");
                    //return Json("Dados Informado Incorretos");
                }

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }

        [Route("api/v1/Notificacao/Buscar")]
        public HttpResponseMessage Buscar(Notificacao model)
        {
            try
            {
                ServicoNotificacao servico = new ServicoNotificacao();

                if (ValidarRequisicaoBuscar(model))
                {
                    var result = servico.Buscar(model);

                    if (result != null)
                    {
                        return Request.CreateResponse<dynamic>(HttpStatusCode.OK, result);
                        //return Json(result);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "sem dados");
                        //return Json("sem dados");
                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, "Favor Preencher Login e Senha");
                    //return Json("Favor Preencher Login e Senha");
                }

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }

        [Route("api/v1/Notificacao/Atualizar")]
        public HttpResponseMessage Atualizar(Notificacao model)
        {
            try
            {
                ServicoNotificacao servico = new ServicoNotificacao();

                if (ValidarRequisicaoAtualizar(model))
                {
                    var result = servico.Atualizar(model);

                        return Request.CreateResponse<dynamic>(HttpStatusCode.OK, result);

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, "Favor Preencher Email e Id");
                }

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }

        private bool ValidarRequisicao(Notificacao model)
        {
            bool ret = false;

            if (model.Email == null)
                ret = false;
            else if (model.Descricao == null)
                ret = false;
            else if (model.Tipo == null)
                ret = false;
            else
                ret = true;

            return ret;
        }

        private bool ValidarRequisicaoBuscar(Notificacao model)
        {
            bool ret = false;

            if (model.Email == null)
                ret = false;
            else
                ret = true;

            return ret;
        }

        private bool ValidarRequisicaoAtualizar(Notificacao model)
        {
            bool ret = false;

            if (model.Email == null)
                ret = false;
            else
                ret = true;

            return ret;
        }


    }
}