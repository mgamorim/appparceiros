﻿using App.Model.SMS;
using App.Model.Usuario;
using App.Servicos.SMS;
using App.Servicos.Usuario;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace App.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CadastrosController : ApiController
    {
        //GET /customers/231/projects/4
        [Route("api/v1/Cadastros/Cadastro")]
        public HttpResponseMessage Cadastro(UsuarioModel model)
        {                        
            try
            {
                if (ValidarRequisicao(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    model.Token = GeraToken().ToString();
                    model.Slug = RetornaSlug(model.Nome).ToLower();

                    var ModelDivulgue = BuscaDadosDivulgacao(model);


                    var retorno = servico.CadastrarUsuario(model, ModelDivulgue);

                    /// ENVIAR TOKEN PARA O CLIENTE TokenAtual
                    /// 
                    ServicoSMS sms = new ServicoSMS();
                    SMS dadosSMS = new SMS();

                    dadosSMS.Mensagem = "Ola! para Iniciar precisamos que confirme o Token de Acesso: " + model.Token;
                    dadosSMS.Numero = model.Telefone.Replace("(","").Replace(")", "").Replace("-", "").Trim();
                    dadosSMS.Senha = "010203";
                    dadosSMS.Titulo = "REDE Evida";

                    sms.EnviarSMS(dadosSMS);

                    dynamic response = null;
                    if(retorno.Equals("Sucesso"))
                        response = Request.CreateResponse(HttpStatusCode.OK, "Enviamos um SMS com o Token para o Telefone (**)****-" + model.Telefone.Substring(7));
                    else
                        return Request.CreateResponse(HttpStatusCode.Created, "Ops! Algo deu errado no seu cadastro.");

                    return response;
                    //return Json(retorno);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                    //return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.Created, "Ops! Algo deu errado.");
                //return Json("");
            }

            //return response;
        }

        [Route("api/v1/Cadastros/ValidarToken")]
        public HttpResponseMessage ValidarToken(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicaoToken(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var ExisteSlug = servico.BuscarClientePorEmail(model);

                    if (ExisteSlug.Token == model.Token)
                    {

                        model.Ativo = 1;
                        model.ConfirmaToken = "S";
                        servico.AtualizaClienteAtivo(model);


                        ServicoSMS sms = new ServicoSMS();
                        SMS dadosSMS = new SMS();

                        dadosSMS.Mensagem = "Seja Bem vindo(a) " + TrataNome(ExisteSlug.Nome)  + "! Agora você é um(a) parceiro(a) de divulgação da REDE Evida!";
                        dadosSMS.Numero = ExisteSlug.Telefone;
                        dadosSMS.Senha = "010203";
                        dadosSMS.Titulo = "REDE Evida";

                        sms.EnviarSMS(dadosSMS);

                        dadosSMS.Mensagem = "So precisa divulgar, nos fechamos a venda para voce! indique um(a) Parceiro(a) e ganhe mensalmente em todas as vendas, Vamos começar?";
                        sms.EnviarSMS(dadosSMS);

                        //dadosSMS.Mensagem = "Faca o Login em https://app.evida.online e compartilhe seu Material de Divulgacao, Acompanhe em tempo real o sucesso no seu Dashboard!";
                        //sms.EnviarSMS(dadosSMS);

                        //if (retornoBoasVindas)
                        //{
                        //    dadosSMS.Mensagem = "Link de Vendas - Segue Seu Link de Vendas Exclusivo https://evida.online/" + ExisteSlug.Slug;
                        //    sms.EnviarSMS(dadosSMS);

                        //    dadosSMS.Mensagem = "Link de Parceiros - Segue Seu Link de Convite para cadastro de parceiros em sua rede https://parceiros.evida.online/" + ExisteSlug.Slug;
                        //    sms.EnviarSMS(dadosSMS);
                        //}


                        return Request.CreateResponse(HttpStatusCode.OK, "Token Validado com Sucesso!");
                        //return Json("Token Validado com Sucesso!");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "Token Inválido!");
                        //return Json("Token Inválido!");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                    //return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.Created, "Ops! Alguma coisa deu Errado!");
                //return Json("");
            }
        }

        [Route("api/v1/Cadastros/RecuperarSenha")]
        public HttpResponseMessage RecuperarSenha(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicaoToken(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {
                        model.Token = GeraToken().ToString();
                        model.Ativo = 0;
                        model.ConfirmaToken = "N";
                        servico.AtualizaClienteAtivo(model);

                        // disparo SMS COM o Token  para o cliente
                        ServicoSMS sms = new ServicoSMS();
                        SMS dados = new SMS();

                        dados.Numero = Cliente.Telefone;
                        dados.Mensagem = "Token de Redefinição de Senha: " + model.Token;
                        dados.Senha = "010203";
                        dados.Titulo = "REDE Evida";


                        sms.EnviarSMS(dados);

                        return Request.CreateResponse(HttpStatusCode.OK, "Enviamos um SMS com o Token para o Telefone (**)****-" + Cliente.Telefone.Substring(7));
                        //return Json("Enviamos um SMS com o Token para o Telefone (**)****-" + Cliente.Telefone.Substring(7));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "Erro Interno!");
                        //return Json("Erro Interno!");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                    //return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }

        [Route("api/v1/Cadastros/ReenviarSMS")]
        public HttpResponseMessage ReenviarSMS(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicaoToken(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {
                        // disparo SMS COM o Token  para o cliente
                        ServicoSMS sms = new ServicoSMS();
                        SMS dados = new SMS();

                        dados.Numero = Cliente.Telefone;
                        dados.Mensagem = "Ola! para Iniciar precisamos que confirme o Token de Acesso: " + Cliente.Token;
                        dados.Senha = "010203";
                        dados.Titulo = "REDE Evida";


                        sms.EnviarSMS(dados);

                        return Request.CreateResponse(HttpStatusCode.OK, "Enviamos um SMS com o Token para o Telefone (**)****-" + Cliente.Telefone.Substring(7));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "Erro Interno!");
                        //return Json("Erro Interno!");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                    //return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }



        [Route("api/v1/Cadastros/CriarNovaSenha")]
        public HttpResponseMessage CriarNovaSenha(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicaoNovaSenha(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    servico.AtualizaSenhaCliente(model);

                    return Request.CreateResponse(HttpStatusCode.OK, "Senha Atualizada com Sucesso");
                    //return Json("Senha Atualizada com Sucesso");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                    //return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }
               
        private int GeraToken()
        {
            int token = 0;

            var chars = "0123456789";
            var random = new Random();
            var TokenAtual = new string(Enumerable.Repeat(chars, 4).Select(s => s[random.Next(s.Length)]).ToArray());
            token = Convert.ToInt32(TokenAtual);


            return token;
        }

        private bool ValidarRequisicao(UsuarioModel model)
        {
            bool ret = false;
            ServicoUsuario servico = new ServicoUsuario();

            var ValidClienteEmail = servico.BuscarClientePorEmail(model);
            var ValidClienteTelefone = servico.BuscarClientePorTelefone(model);
            //var ValidClienteCPF = servico.BuscarClientePorCPF(model);


            if (model.Nome == null)
            {
                ret = false;
                model.Erro = "Digite Nome de usuário!";
            }
            else if (model.Telefone == null)
            {
                ret = false;
                model.Erro = "Digite Numero de Telefone!";
            }
            else if (model.Telefone.Replace("(", "").Replace(")", "").Replace("-", "").Trim().Length < 10)
            {
                ret = false;
                model.Erro = "Numero de Telefone não está no Formato correto!";
            }
            else if (ValidClienteTelefone != null)
            {
                ret = false;
                model.Erro = "Este Telefone já está Vinculado em outra Conta!";
            }
            else if (model.Email == null)
            {
                ret = false;
                model.Erro = "Digite o Email!";
            }
            else if (ValidClienteEmail != null)
            {
                ret = false;
                model.Erro = "Este Email já está Vinculado em outra Conta!";
            }
            //else if (model.DataNascimento == null)
            //{
            //    ret = false;
            //    model.Erro = "Digite a Data de Nascimento!";
            //}
            //else if (model.CPF == null)
            //{
            //    ret = false;
            //    model.Erro = "Digite o CPF!";
            //}
            //else if (model.CPF.Replace(".", "").Replace("-", "").Trim().Length < 11)
            //{
            //    ret = false;
            //    model.Erro = "CPF não está no formato correto!";
            //}
            //else if (ValidClienteCPF != null)
            //{
            //    ret = false;
            //    model.Erro = "Este CPF já Possui uma Conta Associada!";
            //}
            else if (model.Senha == null)
            {
                ret = false;
                model.Erro = "Informe a Senha!";
            }
            else
            {
                ret = true;
            }

            return ret;
        }

        private bool ValidarRequisicaoToken(UsuarioModel model)
        {
            bool ret = false;

            if (string.IsNullOrEmpty(model.Email))
            {
                ret = false;
                model.Erro = "Digite o Email!";
            }
            else
            {
                ret = true;
            }

            return ret;
        }

        private bool ValidarRequisicaoNovaSenha(UsuarioModel model)
        {
            bool ret = false;

            if (model.Email == null)
            {
                ret = false;
                model.Erro = "Email do usuario não encontrado!";
            }
            if (model.Senha == null)
            {
                ret = false;
                model.Erro = "Digite a Senha!";
            }
            else if (model.ConfirmarSenha == null)
            {
                ret = false;
                model.Erro = "Confirme a Nova Senha!";
            }
            else if (model.ConfirmarSenha != model.Senha)
            {
                ret = false;
                model.Erro = "As Senhas não Coincidem!";
            }
            else
            {
                ret = true;
            }

            return ret;
        }

        private DadosDivulgacao BuscaDadosDivulgacao(UsuarioModel usuario)
        {
            DadosDivulgacao dados = new DadosDivulgacao();
            ServicoUsuario servico = new ServicoUsuario();


            dados.IdCliente = usuario.Id;
            dados.LinkVendas = "https://evida.online/" + usuario.Slug;
            dados.ImgBanner1 = "https://parceiros.evida.online/material/evida-imgbanner1.png";
            dados.ImgBanner2 = "https://parceiros.evida.online/material/evida-imgbanner2.png";
            dados.ImgBanner3 = "https://parceiros.evida.online/material/evida-imgbanner3.png";
            dados.LinkVideo1 = "https://www.youtube.com/embed/Mh2_ZlAdKmw";
            dados.LinkCadastroIndicador = "https://parceiros.evida.online/" + usuario.Slug;
            dados.ImgBannerIndicador = "https://parceiros.evida.online/material/evida-imgbannerIndicador.png";
            var url = "https://qrcode.tec-it.com/API/QRCode?size=medium&method=base64&data=https://evida.online/" + usuario.Slug;
            dados.QrCode = servico.BuscaQrCode(url);
            dados.QrCodeCadastro = servico.BuscaQrCode("https://qrcode.tec-it.com/API/QRCode?size=medium&method=base64&data=" + dados.LinkCadastroIndicador);

            return dados;
        }

        private string RetornaSlug(string Nome)
        {
            string slug = string.Empty;
            ServicoUsuario servico = new ServicoUsuario();

            var FormatNome = Nome.Trim().Split(' ');
            slug = FormatNome[0];
            slug = removerAcentos(slug);

            var ExisteSlug = false;
            int i = 0;

            do
            {
                slug = removerAcentos(slug);
                ExisteSlug = servico.BuscaSlug(slug);

                if (ExisteSlug != false)
                {
                    if (i == 0)
                    {
                        slug = FormatNome[FormatNome.Length - 1].ToLower();
                        i++;
                    }
                    else
                    {
                        slug = FormatNome[0].ToLower() + 0 + i;
                        i++;
                    }
                }

            } while (ExisteSlug);

            return slug;
        }


        private string TrataNome(string NomeCompleto)
        {
            string nome = "";

            var nometodo = NomeCompleto.Split();

            if (nometodo.Length > 1)
            {
                nome = nometodo[0] + " " + nometodo[nometodo.Length - 1];
            }

            return nome.ToUpper();
        }

        public static string removerAcentos(string texto)
        {
            string comAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç";
            string semAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc";

            for (int i = 0; i < comAcentos.Length; i++)
            {
                texto = texto.Replace(comAcentos[i].ToString(), semAcentos[i].ToString());
            }
            return texto;
        }

    }
}
