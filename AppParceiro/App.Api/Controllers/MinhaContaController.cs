﻿using App.Model.Usuario;
using App.Servicos.Usuario;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace App.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MinhaContaController : ApiController
    {
        // GET: MinhaConta
        //public ActionResult Index()
        //{
        //    return View();
        //}



        [Route("api/v1/MinhaConta/AtualizarDados")]
        public HttpResponseMessage AtualizarDados(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicao(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {
                        Cliente.Nome = model.Nome;
                        Cliente.Telefone = model.Telefone;
                        Cliente.DataNascimento = model.DataNascimento;
                        Cliente.CPF = model.CPF;
                        Cliente.Senha = model.Senha;
                        Cliente.Imagem = model.Imagem;

                        Cliente.dadosBancarios.NomeBanco = model.dadosBancarios.NomeBanco;
                        Cliente.dadosBancarios.Agencia = model.dadosBancarios.Agencia;
                        Cliente.dadosBancarios.Conta = model.dadosBancarios.Conta;
                        Cliente.dadosBancarios.TipoConta = model.dadosBancarios.TipoConta;
                        Cliente.dadosBancarios.NomeFavorecido = model.dadosBancarios.NomeFavorecido;

                        Cliente.dadosEndereco.Endereco = model.dadosEndereco.Endereco;
                        Cliente.dadosEndereco.Cidade = model.dadosEndereco.Cidade;
                        Cliente.dadosEndereco.Estado = model.dadosEndereco.Estado;
                        Cliente.dadosEndereco.Cep = model.dadosEndereco.Cep;

                        servico.AtualizaDados(Cliente);

                        return Request.CreateResponse<UsuarioModel>(HttpStatusCode.OK, model);
                        //return Json("Atualizado com Sucesso");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "Email não Cadastrado!");
                        //return Json("Email não Cadastrado!");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                    //return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }

        [Route("api/v1/MinhaConta/BuscarDados")]
        public HttpResponseMessage BuscarDados(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicao(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {
                        Cliente.Senha = null;
                        Cliente.Token = null;
                        Cliente.dadosBancarios.Cpf = Cliente.CPF;

                        return Request.CreateResponse<UsuarioModel>(HttpStatusCode.OK, Cliente);
                        //return Json(Cliente);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "Email não Cadastrado!");
                        //return Json("Email não Cadastrado!");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                    //return Json(model.Erro);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }





        [Route("api/v1/MinhaConta/BuscarDivulgacao")]
        public HttpResponseMessage BuscarDivulgacao(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicao(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {
                        var Divulgue = servico.BuscarDadosDivulgacao(Cliente.Id);

                        return Request.CreateResponse(HttpStatusCode.OK, Divulgue);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "Email não Cadastrado!");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [Route("api/v1/MinhaConta/BuscarDashboard")]
        public HttpResponseMessage BuscarDashboard(UsuarioModel model)
        {
            try
            {
                if (ValidarRequisicao(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {
                        var Dashboard = servico.BuscarDashboard(Cliente);

                        Dashboard.IdCliente = Cliente.Id;
                        Dashboard.Email = Cliente.Email;

                        return Request.CreateResponse(HttpStatusCode.OK, Dashboard);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "Email não Cadastrado!");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }






        [Route("api/v1/MinhaConta/Buscarconfig")]
        public HttpResponseMessage Buscarconfig(ConfigModel modelConfig)
        {
            try
            {
                UsuarioModel model = new UsuarioModel();
                model.Email = modelConfig.email;

                if (ValidarRequisicao(model))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {

                        var Config = servico.BuscarConfig(modelConfig);


                        var RetConfig = new ConfigRet();

                        RetConfig.email = Config[0].email;
                        RetConfig.Erro = Config[0].Erro;
                        RetConfig.config = new System.Collections.Generic.List<config>();

                        foreach (var item in Config)
                        {
                            RetConfig.config.Add(new config { Ativo = item.config.Ativo, tipo = item.config.tipo  , id = item.config.id });
                        }


                        return Request.CreateResponse(HttpStatusCode.OK, RetConfig);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "Email não Cadastrado!");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [Route("api/v1/MinhaConta/Atualizaconfig")]
        public HttpResponseMessage Atualizaconfig(ConfigRet modelConfig)
        {
            try
            {
                UsuarioModel model = new UsuarioModel();
                model.Email = modelConfig.email;

                if (ValidarRequisicaoConfig(modelConfig))
                {
                    ServicoUsuario servico = new ServicoUsuario();

                    var Cliente = servico.BuscarClientePorEmail(model);

                    if (Cliente != null)
                    {

                        var Config = servico.AtualizaConfig(modelConfig);


                        return Request.CreateResponse(HttpStatusCode.OK, Config);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "Email não Cadastrado!");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, model.Erro);
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }


        [Route("api/v1/MinhaConta/BuscaTreinamento")]
        public HttpResponseMessage BuscaTreinamento(UsuarioModel model)
        {
            try
            {
                ServicoUsuario servico = new ServicoUsuario();

                if (ValidarRequisicao(model))
                {
                    var result = servico.BuscarTreinamento(model);

                    if (result != null)
                    {
                        return Request.CreateResponse<dynamic>(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Created, "sem dados");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Created, "Favor Preencher Email");
                }

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }



        private bool ValidarRequisicao(UsuarioModel model)
        {
            bool ret = false;
            ServicoUsuario servico = new ServicoUsuario();



             if (model.Email == null)
            {
                ret = false;
                model.Erro = "Email Invalido!";
            }
            else
            {
                ret = true;
            }

            return ret;
        }

        private bool ValidarRequisicaoConfig(ConfigRet model)
        {
            bool ret = false;
            ServicoUsuario servico = new ServicoUsuario();


            if (model.email == null)
            {
                ret = false;
                model.Erro = "Email Invalido!";
            }
             else if (model.config == null)
            {
                ret = false;
                model.Erro = "Config Invalido!";
            }
            else
            {
                ret = true;
            }

            return ret;
        }

    }
}