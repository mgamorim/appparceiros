﻿using App.Model.Usuario;
using App.Servicos.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace App.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HankingController : ApiController
    {

        [Route("api/v1/Hanking/BuscarHankingVendas")]
        public HttpResponseMessage BuscarHankingVendas()
        {
            try
            {
               // var teste = Convert.ToDecimal("740,00");



                HankingModel model = new HankingModel();
                ServicoHanking servico = new ServicoHanking();

                var retorno = servico.BuscarHankingVendas();

                var ordernacao = retorno.OrderByDescending(x => x.TotalComissao).ThenByDescending(x => x.TotalIndicados).ToList();
                

                List<HankingModel> ret = new List<HankingModel>();

                for (int i = 0; i < (ordernacao.Count > 100 ? 100 : ordernacao.Count); i++)
                {
                    ordernacao[i].Posicao = i + 1;

                    ret.Add(ordernacao[i]);
                }

                return Request.CreateResponse(HttpStatusCode.OK, ret);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }


        [Route("api/v1/Hanking/BuscarHankingVendasIndicados")]
        public HttpResponseMessage BuscarHankingVendasIndicados()
        {
            try
            {
                HankingModel model = new HankingModel();
                ServicoHanking servico = new ServicoHanking();

                var retorno = servico.BuscarHankingVendasIndicados();

                var ordernacao = retorno.OrderByDescending(x => x.TotalComissao).ThenByDescending(x => x.TotalIndicados).ToList();


                List<HankingModel> ret = new List<HankingModel>();

                for (int i = 0; i < (ordernacao.Count > 100 ? 100 : ordernacao.Count); i++)
                {
                    ordernacao[i].Posicao = i + 1;

                    ret.Add(ordernacao[i]);
                }

                return Request.CreateResponse(HttpStatusCode.OK, ret);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }

        [Route("api/v1/Hanking/BuscarHankingDestaqueMes")]
        public HttpResponseMessage BuscarHankingDestaqueMes()
        {
            try
            {
                HankingModel model = new HankingModel();
                ServicoHanking servico = new ServicoHanking();

                var retorno = servico.BuscarHankingDestaqueMes();

                var ordernacao = retorno.OrderByDescending(x => x.TotalComissao).ThenByDescending(x => x.TotalIndicados).ToList();


                List<HankingModel> ret = new List<HankingModel>();

                for (int i = 0; i < (ordernacao.Count > 100 ? 100 : ordernacao.Count); i++)
                {
                    ordernacao[i].Posicao = i + 1;

                    ret.Add(ordernacao[i]);
                }

                return Request.CreateResponse(HttpStatusCode.OK, ret);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
                //return Json("");
            }
        }

    }
}
