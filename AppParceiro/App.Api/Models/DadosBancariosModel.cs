﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Api.Models
{
    public class DadosBancariosModel
    {
        public int IdCliente { get; set; }
        public string NomeBanco { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string TipoConta { get; set; }
        public string NomeFavorecido { get; set; }
        public string CpfFavorecido { get; set; }
    }
}