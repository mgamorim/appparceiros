﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Api.Models
{
    public class DashboardModel
    {
        public int IdCliente { get; set; }

        public string FaturamentoMensal { get; set; }
        public string FaturamentoTotal { get; set; }

        public string ValorAReceber { get; set; }



        public string QtdSimulacao { get; set; }
        public string QtdPropostas { get; set; }
        public string QtdPropostasPendentes { get; set; }
        public string QtdEmHomologacao { get; set; }
        public string Rejeitadas { get; set; }
        public string PendentePgto { get; set; }



        public string QtdIndicados { get; set; }
        public string QtdPropostasIndicados { get; set; }
        public string PendentePgtoIndicados { get; set; }
        public string ValorAReceberIndicados { get; set; }

    }
}